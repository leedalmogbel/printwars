@extends('seller.partials.frame')

@section('content')
    <div class="card">
        <div class="card-header">
        Products
        </div>
        <div class="card-body">
            <form>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Search Product Title or SKU</span>
                    </div>
                    <input type="text" class="form-control"
                           name="q"
                           placeholder="Search here"
                           value="{{ request()->get('q') }}" />
                    
                    <div class="input-group-append">
                        <button class="btn btn-success" type="submit"><span class="fa fa-search"></span> Search</button>
                    </div>
                </div>
            </form>
            <br />
            <div class="clearfix">
                <a href="{{ route('seller-product-design') }}" class="float-right btn btn-success"><span class="fa fa-plus"></span> Design a Tshirt</a>
            </div>
            <br />
            <table class="table table-striped table-bordered">
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>SKU</th>
                    <th>Preview</th>
                    <th>Price</th>
                    <th>Status</th>
                    <th>Created</th>
                    <th>Updated</th>
                    <th><center>Action</center></th>
                </tr>
                @foreach ($products as $product)
                    <tr>
                        <td>{{ $product->id }}</td>
                        <td>{{ $product->title }}</td>
                        <td>{{ $product->sku }}</td>
                        <td><img src="{{ $product->image('front') }}" class="img-list" /></td>
                        <td>PHP {{number_format($product->price, 2)}}</td>
                        <td>
                            @if ($product->active == 1)
                                <span class="text-success">Active</span>
                            @else
                                <span class="text-danger">Inactive</span>
                            @endif
                            &mdash;
                            @if ($product->status == 1)
                                <span class="text-info">Pending</span>
                            @elseif ($product->status == 2)
                                <span class="text-success">Approved</span>
                            @else
                                <span class="text-danger">Rejected</span>
                            @endif
                        </td>
                        <td>{{ date('M d, Y H:i:s', strtotime($product->created_at)) }}</td>
                        <td>{{ date('M d, Y H:i:s', strtotime($product->updated_at)) }}</td>
                        <td>
                            <center>
                                <a href="{{ route('seller-product-detail', $product->id) }}" title="View"><span class="fa fa-eye text-info"></span></a>
                                @if ($product->active == 1)
                                    <a href="#" title="Delete" class="remove-product" data-id="{{ $product->id }}">
                                        <span class="fa fa-trash text-danger"></span>
                                    </a>
                                @else
                                    <a href="#" title="Activate" class="activate-product" data-id="{{ $product->id }}">
                                        <span class="fa fa-check text-success"></span>
                                    </a>
                                @endif
                            </center>
                        </td>
                    </tr>
                @endforeach
            </table>
            <br />
            {{ $products->appends(Request::except('page'))->links() }}
        </div>
    </div>
@endsection

@section('custom-css')
    <link type="text/css" rel="stylesheet" href="/components/jquery-confirm/dist/jquery-confirm.min.css" />
@endsection
@section('custom-scripts')
    <script type="text/javascript" src="/components/jquery-confirm/dist/jquery-confirm.min.js"></script>
    <script>
     window.page = 'product';
     $('.remove-product').click(function (e) {
         e.preventDefault();
         let id = $(this).data('id');
         $.confirm({
             title : 'Deactivate Product',
             type: 'red',
             content : 'Are you sure you want to deactivate this product?',
             buttons : {
                 Yes:  {
                     btnClass : 'btn btn-danger',
                     action: function() {
                         window.location.href='{{ route('seller-product-remove', ':id') }}'.replace(':id', id);
                     }
                 },
                 Cancel: function() {

                 }
             }
         });
     });
     
     $('.activate-product').click(function (e) {
         e.preventDefault();
         let id = $(this).data('id');
         $.confirm({
             title : 'Activate Product',
             type: 'green',
             content : 'Are you sure you want to activate this product?',
             buttons : {
                 Yes: {
                     btnClass: 'btn btn-success',
                     action: function() {
                         window.location.href='{{ route('seller-product-activate', ':id') }}'.replace(':id', id);
                     }
                 },
                 Cancel: function() {

                 }
             }
         });
     });
    
    </script>
@endsection
