<div class="card order-list">
    <div class="card-header">Orders</div>
    <div class="card-body">
        <form>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">Search By</span>
                </div>
                <div class="input-group-prepend">
                    <select name="filter" class="order-filter">
                        <option{{request()->get('filter') == 'order_id' ? ' selected' : null}} value="order_id">Order ID</option>
                        <option{{request()->get('filter') == 'sku' ? ' selected' : null}} value="sku">Product SKU</option>
                        <option{{request()->get('filter') == 'name' ? ' selected' : null}} value="name">Product Title</option>
                    </select>
                </div>
                <input type="text" class="form-control"
                       name="q"
                       placeholder="Search here..."
                       value="{{ request()->get('q') }}" />
                
                <div class="input-group-append">
                    <button class="btn btn-success" type="submit"><span class="fa fa-search"></span> Search</button>
                </div>
            </div>
        </form>
        <br />
        @if ($status == 'payable' && $page == 'seller')
            <div class="text-right bulk-payable">
                <a href="#" class="btn btn-warning">PHP <span>0.00</span> &mdash; Pay with <i class="fab fa-paypal"></i>aypal</a>
            </div>
        @endif
        <ul class="nav nav-tabs">
            <li class="nav nav-item">
                <a href="{{route($page . '-order-list', 'all')}}" class="nav-link{{$status == 'all' ? ' active': ''}}">All</a>
            </li>
            @foreach (App\Quote::statuses()->all() as $stat)
                <li class="nav nav-item">
                    <a href="{{route($page .'-order-list', $stat['key'])}}" class="nav-link{{$status == $stat['key'] ? ' active': ''}}">{{ $stat['name'] }}</a>
                </li>
            @endforeach
        </ul>
        <br />
        <br />
        <br />
        @foreach ($orders as $item)
            <div class="row">
                <div class="col-md">
                    <div><b class="text-primary">Order No. #{{$item->invoice}}</b></div>
                    <div>
                        <input type="checkbox"
                               data-payable="{{ $item->getPayable() }}"
                               class="check-select"
                               name="q[]"
                               value="{{$item->cart_id}}" />
                    </div>
                </div>
                <div class="col-md">
                    <img src="{{$item->product()->first()->image('front')}}" class="product-img" />
                </div>
                <div class="col-md">
                    <div>
                        @if ($page == 'seller')
                            <a href="{{$item->product()->first()->detailLink}}">
                                <strong>{{$item->product()->first()->title}}</strong>
                            </a>
                        @else
                            <a href="{{route('admin-product-detail', $item->product()->first()->id)}}">
                                <strong>{{$item->product()->first()->title}}</strong>
                            </a>
                        @endif
                    </div>
                    <div class="text-secondary">sku:
                        <span class="text-danger">{{$item->product()->first()->sku}}</span>
                    </div>
                    <div class="text-secondary">status:
                        <span class="text-info">{{$item->resolveStatus('name')}}</span>
                    </div>
                    <div class="text-secondary">size:
                        <span class="text-success">{{$item->actualSize()}}</span>
                    </div>
                    <div class="text-secondary">qty:
                        <span class="text-success">x {{$item->qty}}</span>
                    </div>
                </div>
                <div class="col-md">
                    @if($page == 'seller')
                        <div class="text-secondary">payable:
                            <span class="text-danger">PHP {{number_format($item->getPayable(), 2)}}</span>
                        </div>
                        <div class="text-secondary">your earning:
                            <span class="text-success">PHP {{number_format($item->getEarnings(), 2)}}</span>
                        </div>
                        <br />
                        @if($item->status == 1)
                            <div>
                                <a href="{{route('seller-order-paynow', $item->cart_id)}}" class="btn btn-primary pay-now"><span class="fab fa-paypal"></span> Pay via Paypal</a>
                            </div>
                            <br />
                            <div>
                                <a href="#" data-id="{{$item->cart_id}}" class="btn btn-danger cancel-order">&times; Cancel Order</a>
                            </div>
                        @endif
                    @else
                        @if($item->status == 2)
                            <div>
                                <a href="#" class="btn btn-warning move-to" data-val="for-shipping" data-id="{{$item->cart_id}}">
                                    <span class="fa fa-truck"></span> Move for Shipping
                                </a>
                            </div>
                        @elseif($item->status == 3)
                            <div>
                                <a href="#" class="btn btn-warning move-to" data-val="shipped" data-id="{{$item->cart_id}}">
                                    <span class="fa fa-truck"></span> Move to Shipped
                                </a>
                            </div>
                        @elseif($item->status == 4)
                            <div>
                                <a href="#" class="btn btn-warning move-to" data-val="delivered" data-id="{{$item->cart_id}}">
                                    <span class="fa fa-truck"></span> Move to Delivered
                                </a>
                            </div>
                        @endif
                    @endif
                </div>
            </div>
            <br />
            <hr />
        @endforeach
        <br />
        <br />
        {{$orders->appends(Request::except('page'))->links()}}
    </div>
</div>
