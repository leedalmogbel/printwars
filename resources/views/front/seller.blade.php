@extends('front.partials.frame')

@section('content')
    <br />
    <div class="search-page container">
        <br />
        <h1>{{$user->name}}</h1>
        <div class="text-secondary">Sellers t-shirt design.</div>
        <hr />
        <div class="product-slider">
            <div class="row slider-body">
                @foreach($products as $product)
                @include('front.partials.product.grid', ['product' => $product])
                @endforeach
            </div>
        </div>
        <br />
        {{$products->appends(Request::except('page'))->links()}}
    </div>
@endsection
