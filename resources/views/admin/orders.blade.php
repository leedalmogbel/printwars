@extends('admin.partials.frame')

@section('content')
    @include('globals.orders', ['page' => 'admin'])
@endsection

@section('custom-css')
    <link type="text/css" rel="stylesheet" href="/components/jquery-confirm/dist/jquery-confirm.min.css" />
@endsection

@section('custom-scripts')
    <script type="text/javascript" src="/components/jquery-confirm/dist/jquery-confirm.min.js"></script>
    <script type="text/javascript">
     $('.move-to').click(function(e) {
         e.preventDefault();
         let id = $(this).data('id');
         let status = $(this).data('val');
         let statusUrl = '{{route('admin-quote-statuses', ':key')}}'.replace(':key', status);
         $.get(statusUrl, function(res) {
             $.confirm({
                 title: 'Move Item to "' + res + '"',
                 content: 'Move item to "' + res + '"',
                 type: 'blue',
                 buttons: {
                     Yes : {
                         btnClass: 'btn btn-primary',
                         action: function() {
                             window.location.href="{{route('admin-order-move', [':id', ':key'])}}".replace(':id', id).replace(':key', status);
                         }
                     },
                     No : function() {}
                 }
             });
         }).fail(function() {
             toastr['error']('Something went wrong');
         });
     });
    </script>
@endsection
