<?php

namespace App\Http\Controllers\Admin;

use Request;
use Session;
use App\Http\Controllers\Controller;
use App\User as UserModel;
use Redirect;

class User extends Controller
{
    const TYPE = 'admin';
    
    //
    public function login () {
        return view('admin.login');
    }

    public function signin () {
        $loginData = Request::validate(['email' => 'required|email',
            'password' => 'required'
        ]);

        try {
            // login
            $seller = UserModel::login($loginData, self::TYPE);
            
            return Redirect::route('admin-dash');
        } catch (\Exception $e) {
            $this->setToastrMsg($e->getMessage(), 'error');
            return Redirect::route('admin-login')->withInput(['email' => $loginData['email']])
                ->withErrors(['email' => $e->getMessage()]);
        }
    }

    public function sellerList() {
        $data = [];
        $data['sellers'] = UserModel::where('type', 'seller')
            ->orderBy('updated_at', 'DESC')
            ->paginate(10);

        return view('admin.sellers', $data);
    }

    public function sellerDisable(UserModel $user) {
        if ($user->type != 'seller') {
            $this->setToastrMsg('Seller not found', 'error');
            return redirect()->back();
        }

        $user->status = 0;
        $user->save();

        $this->setToastrMsg('Seller Updated', 'success');
        return redirect()->back();
    }

    public function sellerActivate(UserModel $user) {
        if ($user->type != 'seller') {
            $this->setToastrMsg('Seller not found', 'error');
            return redirect()->back();
        }

        $user->status = 2;
        $user->save();

        $this->setToastrMsg('Seller Updated', 'success');
        return redirect()->back();
    }
}
