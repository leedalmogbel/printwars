<?php

namespace App\Http\Controllers\Front;

use Request;
use Session;
use App\Http\Controllers\Controller;

use App\Product;
use App\Quote;

class Cart extends Controller
{
    //
    public function add() {
        $data = Request::validate(['id' => 'required',
            'size' => 'required',
            'qty' => 'required']);

        $data = array_values($data);
        try {
            Quote::addToCart(...$data);
        } catch (\Exception $e) {
            $this->setToastrMsg($e->getMessage(), 'error');
            return redirect()->route('cart');
        }

        $this->setToastrMsg('Item added to cart', 'success');
        return redirect()->route('cart');
    }

    public function index() {
        $data = [];
        $data['cart'] = Quote::getCartItems(Session::getId());
        
        return view('front.cart', $data);
    }

    public function update(Quote $quote) {
        if (empty($quote) || $quote->session != Session::getId() || $quote->invoice != 0) {
            $this->setToastrMsg('Invalid request', 'error');
            return redirect()->back();
        }

        try {
            $quote->updateQuoteQty(Request::get('qty'));
        } catch (\Exception $e) {
            $this->setToastrMsg($e->getMessage(), 'error');
            return redirect()->back();
        }

        return $this->index();
    }

    public function remove(Quote $quote) {
        if (empty($quote) || $quote->session != Session::getId() || $quote->invoice != 0) {
            $this->setToastrMsg('Something went wrong', 'error');
            return redirect()->route('cart');
        }

        // TODO: Dont delete entry in the database
        $quote->delete();

        $this->setToastrMsg('Cart items updated', 'success');
        return redirect()->route('cart');
    }
}
