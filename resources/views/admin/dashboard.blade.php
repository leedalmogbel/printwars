@extends('admin.partials.frame')
@section('content')
    <div class="wrapper">
        <h1>DASHBOARD</h1>
        <hr />
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">Your latest designs</div>
                    <div class="card-body">
                        Lorem ipsum dolor set amit
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card">
                    <div class="card-header">Best seller</div>
                    <div class="card-body">
                        Lorem ipsum dolor set amit
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card">
                    <div class="card-header">Approved designs</div>
                    <div class="card-body">
                        Lorem ipsum dolor set amit
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom-scripts')
    <script>
     document.body.style.fontFeatureSettings = '"liga" 0';
     window.page = 'dashboard';
    </script>
@endsection
