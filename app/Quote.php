<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Session;
use App\Quote\Shipping;
use App\Repos\Quote\Status;

use App\Events\Quote\Updating as QuoteUpdatingEvent;
use App\Events\Quote\Updated as QuoteUpdatedEvent;

class Quote extends Model
{
    const SIZES = ['xs' => 'Extra Small',
        's' => 'Small',
        'm' => 'Medium',
        'l' => 'Large',
        'xl' => 'Extra Large'];

    const PAYABLE_PER_SHIRT = 400;

    
    protected $dispatchesEvents = [
        'updating' => QuoteUpdatingEvent::class,
        'updated' => QuoteUpdatedEvent::class
    ];

    /**
     * get order count by seller and/or status
     *
     * @param App\User user
     * @param string status
     * @return App\Quotes
     */
    public static function getOrdersBySeller(\App\User $user, $status = null) {
        $obj = self::join('products', 'quotes.product', '=', 'products.id')
            ->join('invoices', 'quotes.invoice', '=', 'invoices.id')
            ->where('invoices.status', '!=', '0')
            ->where('products.user', $user->id);

        if ($status !== null) {
            $obj->where('quotes.status', self::statuses()->getValueByKey($status));
        }

        return $obj;
    }
    
    /**
     * update quantity
     *
     * @param numeric $qty
     * @return numeric
     */
    public function updateQuoteQty($qty) {
        if (empty($this)) {
            throw new \Exception('Cart item doesn\'t exists.');
        }
        
        if (!is_numeric($qty) || $qty < 1 || floor($qty) != $qty) {
            throw new \Exception('Quantity is invalid');
        }

        $this->qty = $qty;
        $this->amount = $this->product()->first()->price * $this->qty;
        $this->shipping = Shipping::calculate($this);
        return $this->save();
    }

    /**
     * returns App\Repos\Quote\Status instance
     *
     * @return App\Repos\Quote\Status
     */
    public static function statuses() {
        return new Status;
    }

    /**
     * get payable amount
     *
     * @return numeric
     */
    public function getPayable() {
        if (empty($this)) {
            return '';
        }

        $discount = $this
            ->product()
            ->first()
            ->user()
            ->first()
            ->tier()
            ->getTierDiscount();

        $discount = $discount * $this->qty;
            
        return (self::PAYABLE_PER_SHIRT * $this->qty) - $discount;
    }

    /**
     * returns delivered order this month by seller
     *
     * @return numeric
     */
    public static function getDeliveredOrderBySellerThisMonth() {
        
        $quotes = Quote::getOrdersBySeller(Session::get('me'), 'delivered')
            ->thisMonth()
            ->get();

        $count = 0;
        
        foreach ($quotes as $quote) {
            $count = $count + $quote->qty;
        }

        return $count;
    }


    /**
     * filters for this month
     *
     * @return App\Quote
     */
    public function scopeThisMonth($query) {
        $m = date('m');
        $y = $ny = date('Y');
        $nm = $m + 1;
        
        if ($m == 12) {
            $nm = 1;
            $ny++;
        }

        return $query->whereRaw('quotes.updated_at BETWEEN ? AND ?', ["$y-$m-01", "$ny-$nm-01"]);
    }

    /**
     * get earning amount
     *
     * @return numeric
     */
    public function getEarnings() {
        return $this->amount - ($this->getPayable());
    }

    /**
     * cancel item by seller
     *
     * @param App\User $seller
     * @return App\Quote
     */
    public function cancelItemBySeller(User $seller) {
        if (empty($this)) {
            throw new \Exception('Invalid order');
        }
        
        if (empty($seller)) {
            throw new \Exception('Invalid Order');
        }

        if ($seller->id != $this->product()->first()->user) {
            throw new \Exception('Invalid order');
        }

        $this->status = self::statuses()->getValueByKey('cancelled');
        $this->save();

        return $this;
    }

    /**
     * returns status name by value
     *
     * @return string
     */
    public function resolveStatus() {
        if (empty($this)) {
            return;
        }

        return self::statuses()->getNameByValue($this->status);
    }

    /**
     * get quotes with invoice filtered by seller
     *
     * @return collection
     */
    public static function getQuoteWithInvoiceBySeller(User $seller, $status) {
        if (empty($seller)) {
            throw new \Exception('Seller is invalid');
        }
        
        return self::getQuoteWithInvoiceQuery($status)
            ->where('products.user', '=', $seller->id)
            ->where('invoices.status', '!=', '0')
            ->where('quotes.invoice', '!=', '0')
            ->orderBy('invoices.id', 'DESC')
            ->paginate(10);
    }

    /**
     * get quotes with invoice
     *
     * @return collection
     */
    public static function getQuoteWithInvoice($stat) {
        return self::getQuoteWithInvoiceQuery($stat)
            ->where('invoices.status', '!=', '0')
            ->where('quotes.invoice', '!=', '0')
            ->orderBy('invoices.id', 'DESC')
            ->paginate(10);
    }

    /**
     * add item to cart
     *
     * @param numeric product id
     * @param string size
     * @param integer qty
     * @return numeric
     */
    public static function addToCart($id, $size, $qty) {
        $product = Product::where('id', $id)->first();
        if (empty($product)) {
            throw new \Exception('Product is invalid');
        }

        if ($product->status != 2 || $product->active != 1) {
            throw new \Exception('Product is out of stock');
        }

        if (!array_key_exists($size, self::SIZES)) {
            throw new \Exception('Size is invalid');
        }

        if ($qty < 1 || floor($qty) != $qty) {
            throw new \Exception('Invalid quantity');
        }

        $obj = new Quote;
        if ($exist = self::getCartExisting($id, $size)) {
            $obj = $exist;
            $qty = $obj->qty + $qty;
        }
        
        $obj->product = $id;
        $obj->qty = $qty;
        $obj->size = $size;
        $obj->invoice = 0;
        $obj->session = Session::getId();
        $obj->amount = $product->price * $qty;
        $obj->shipping = Shipping::calculate($obj);
        return $obj->save();
    }

    /**
     * returns existing cart value
     *
     * @param numeric $id
     * @param string $size
     * @return App\Quote
     */
    public static function getCartExisting($id, $size) {
        return self::where('product', $id)
            ->where('size', $size)
            ->where('session', Session::getId())
            ->where('invoice', 0)
            ->first();
    }

    public static function getCartItems($session) {
        $cart = self::where('session', $session)
            ->where('invoice', 0)
            ->get();

        $total = $shipping = 0;
        foreach ($cart as $item) {
            $total += $item->amount;
            $shipping += $item->shipping;
        }

        $cart->totalAmount = $total;
        $cart->shippingAmount = $shipping;
        return $cart;
    }

    
    public static function cartCount($session) {
        return self::getCartItems($session)->count();
    }
    

    public static function updateQuoteInvoice($sessId, Invoice $invoice) {
        if (empty($invoice)) {
            throw new \Exception('Something went wrong');
        }

        return self::where(['session' => $sessId, 'invoice' => 0])
            ->update(['invoice' => $invoice->id]);
    }

    public function product() {
        return $this->hasOne('App\Product', 'id', 'product');
    }

    public function actualSize() {
        if (empty($this)) {
            return null;
        }

        return self::SIZES[$this->size];
    }

    public static function getQuoteByInvoice(Invoice $invoice) {
        return self::where('invoice', $invoice->id)
            ->get();
    }

    protected static function getQuoteWithInvoiceQuery($status) {

        $query =  self::select('quotes.*',
        'products.*',
        'invoices.*',
        'invoices.created_at as invoice_created',
        'quotes.id as cart_id',
        'quotes.status as status',
        'products.id as p_id',
        'invoices.status as invoice_status')
            ->join('products', 'quotes.product', '=', 'products.id')
            ->join('invoices', 'invoices.id', '=', 'quotes.invoice');
        
        $filter = request()->has('filter') ? request()->get('filter') : null;

        if (request()->has('q') && trim(request()->get('q')) != '') {
            switch($filter) {
                case 'order_id':
                    $query->where('invoices.id', request()->get('q'));
                    break;
                case 'sku':
                    $query->where('products.sku', request()->get('q'));
                    break;
                case 'name':
                    $query->where('products.title', 'like', '%' . request()->get('q') . '%');
                    break;
                default:
                    break;
            }

        }

        if ($status == 'all') {
            return $query;
        }

        return $query->where('quotes.status', '=', self::statuses()->getValueByKey($status));
    }
}
