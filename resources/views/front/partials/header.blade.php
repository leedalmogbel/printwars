<div class="container">
    <nav class="navbar navbar-expand-lg navbar-light">
        <a class="navbar-brand" href="{{route('home')}}">GEAR <span>SHARK</span></a>
        <form class="form-inline search-form" action="{{route('product-search')}}">
            <input type="text" class="form-control"
                   placeholder="Find your design."
                   value="{{Request::get('keyword')}}"
                   name="keyword" />
        </form>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <div class="mr-auto"></div>
            <ul class="navbar-nav my-2 my-lg-0">
                <li class="nav-item active">
                    <a class="nav-link" href="/"><span class="fa fa-home"></span> <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/cart">
                        <span class="fa fa-shopping-basket"></span>
                        <small class="text-primary">[{{ App\Quote::cartCount(Session::getId()) }}]</small>
                    </a>
                </li>
                @if(!Session::has('me'))
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="fa fa-user"></span></a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{route('front-login')}}">Login</a>
                            <a class="dropdown-item" href="{{route('front-signup')}}">Signup</a>
                        </div>
                    </li>
                @else
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="fa fa-user"></span></a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#">Account</a>
                            <a class="dropdown-item" href="{{route('orders')}}">Orders</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{route('logout')}}">Logout</a>
                        </div>
                    </li>
                @endif
            </ul>
        </div>
    </nav>
</div>
<hr />
