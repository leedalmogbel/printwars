<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuoteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('product')->index();
            $table->integer('qty');
            $table->string('size');
            $table->integer('invoice')->default(0)->index();
            $table->string('session')->default();
            $table->integer('payment')->index()->default(0);
            $table->integer('status')->default(1)->index();
            $table->float('shipping', 11, 2);
            $table->float('amount', 11, 2)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotes');
    }
}
