<!doctype html>
<html lang="{{ app()->getLocale() }}" class="{{$class}}">
    <head>
        <title>Login</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link type="text/css" rel="stylesheet" href="/css/app.css" />
        <link type="text/css" rel="stylesheet" href="/components/fontawesome5-fullcss/css/all.css" />
        <link type="text/css" rel="stylesheet" href="/components/toastr/build/toastr.css" />
        <link type="text/css" rel="stylesheet" href="/components/jquery-confirm/dist/jquery-confirm.min.css" />
        <link type="text/css" rel="stylesheet" href="/components/pace-js/themes/blue/pace-theme-flash.css" />
        <link type="text/css" rel="stylesheet" href="/css/login.css" />
        <script type="text/javascript" src="/components/jquery/dist/jquery.min.js"></script>
        <script type="text/javascript" src="/components/toastr/build/toastr.min.js"></script>
        <script type="text/javascript" src="/components/jquery-confirm/dist/jquery-confirm.min.js"></script>
        <script type="text/javascript" src="/components/pace-js/pace.min.js"></script>
        <script type="text/javascript">
         toastr.options = {
             "positionClass": "toast-bottom-left",
         };
        </script>
    </head>
    <body>
        <div class="login-wrapper">
            <div class="login-container">
                <div class="login-div">
                    <div class="float-right"><small><span class="fa fa-lock"></span> {{$type}} LOGIN</small></div>
                    <h4><a href="{{route('home')}}" class="text-secondary">GEAR <small>SHARK</small></a></h4>
                    <br />
                    @if ($errors->has('username') || $errors->has('password'))
                        <p class="text-danger">
                            <span class="fa fa-exclamation-circle"></span> Something went wrong. Please try again.
                        </p>
                    @endif
                    <form method="post" action="{{ route($class . '-login', ['u' => Request::has('u') ? Request::get('u') : '/']) }}">
                        @csrf
                        <div class="form-group">
                            <input type="text" name="email"
                                   class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="email address"
                                   value="{{ old('email') }}" />
                            @if($errors->has('email'))
                                @foreach ($errors->get('email') as $error)
                                    <small class="text-danger">{{ $error }}</small>
                                @endforeach
                            @endif
                        </div>
                        <div class="form-group">
                            <input type="password" name="password"
                                   class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="password" />
                            @foreach ($errors->get('password') as $error)
                                <small class="text-danger">{{ $error }}</small>
                            @endforeach
                        </div>
                        <input type="submit" value="LOGIN" class="btn btn-primary d-block btn-lg" />
                        <br>
                        <div class="float-right">    
                            <a href="/forgot">Forgot Password?</a>
                        </div>
                        @if($type == 'user')
                        <div>
                            <a href="{{route('front-signup')}}">Create an account</a>
                        </div>
                        @endif
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
        <script type="text/javascript">
         @if (Session::has('msg'))
         toastr['{{ Session::has('msg_type') ? Session::get('msg_type') : 'info' }}']('{{ Session::get('msg') }}');
         @endif
        </script>
    </body>
</html>
