@extends('front.partials.frame')

@section('content')
    <br />
    <div class="cart-wrapper container">
        <br />
        <h1>Cart Items</h1>
        <br />
        @if ($cart->isEmpty())
            <p>You don't have any item in your shopping cart.</p>
            <hr />
        @else
        <table class="table table-bordered table-striped table-responsive-xl">
            <tr>
                <th>Product</th>
                <th>Size</th>
                <th>Amount</th>
                <th><center>Quantity</center></th>
                <th><center>Action</center></th>
            </tr>
            @foreach ($cart as $item)
                <tr>
                    <td><a href="{{$item->product()->first()->detailLink}}">{{$item->product()->first()->title}}</a></td>
                    <td><span class="text-secondary">{{$item->actualSize()}}</span></td>
                    <td><span class="text-success">PHP {{number_format($item->amount, 2)}}</span></td>
                    <td>
                        <center>
                            <input type="number" class="qty-val" data-qty="{{$item->qty}}" data-id="{{$item->id}}" value="{{$item->qty}}" />
                        </center>
                    </td>
                    <td>
                        <center>
                            <a href="#" class="remove-cart text-danger" data-id="{{$item->id}}"><span class="fa fa-trash"></span></a>
                        </center>
                    </td>
                </tr>
            @endforeach
        </table>
        @endif
        <br />
        <div class="text-right g-total">
            <div class="text-secondary">Shipping Total: <span class="text-primary">{{number_format($cart->shippingAmount, 2)}}</span></div>
            <div class="text-secondary">Order Total: <span class="text-primary">{{number_format($cart->totalAmount, 2)}}</span></div>
            <div class="text-secondary">Grand Total: <span class="text-primary">{{number_format($cart->totalAmount + $cart->shippingAmount, 2)}}</span></div>
            <br />
            <a href="{{route('home')}}" class="btn btn-secondary">Continue Shopping</a>
            @if (!$cart->isEmpty())
                <a href="{{route('checkout')}}" class="btn btn-primary">Proceed to Checkout</a>
            @endif
        </div>
    </div>
@endsection

@section('custom-css')
    <link type="text/css" rel="stylesheet" href="/components/jquery-confirm/dist/jquery-confirm.min.css" />
    <style>
     .qty-val {
         border: 1px solid #ccc;
         border-radius: 4px;
         padding: 10px;
         text-align: center;
         width: 80px;
     }
    </style>
@endsection

@section('custom-scripts')
    <script type="text/javascript" src="/components/jquery-confirm/dist/jquery-confirm.min.js"></script>
    <script>
     $(document).on('change', '.qty-val', function(e) {
         let params = {
             id : $(this).data('id'),
             qty : $(this).val()
         };

         if (params.qty < 1) {
             $.alert({
                 title : 'Invalid Quantity',
                 type : 'red',
                 content : 'Cannot set quantity to less than 1.'
             });

             $(this).val($(this).data('qty'));
             
             return;
         }

         $('.cart-wrapper').css({'opacity' : '0.6'});
         let self = this;
         $.get('{{route('cart-update', ':id')}}'.replace(':id', params.id), params, function (res) {
             $('.cart-wrapper').css({'opacity' : 1});
             
              try {
                 if (res.type == 'error') {
                     $.alert({
                         title : 'Error',
                         type : 'red',
                         content : res.msg
                     });

                     $(self).val($(self).data('qty'));
                     return;
                 }

                  throw 'success';
                 
             } catch (e) {
                 let htm = $(res).find('.cart-wrapper').html();
                 $('html').find('.cart-wrapper').html(htm);
                 toastr['success']('Cart updated');
             }
         }).fail(function() {
             toastr['error']('Something went wrong. Please try again');
             $('.cart-wrapper').css({'opacity' : 1});
             $(self).val($(self).data('qty'));
         });
         
     }).on('click', '.remove-cart', function (e) {
         e.preventDefault();
         let id = $(this).data('id');
         $.confirm({
             title: 'Remove from cart',
             type: 'red',
             content: 'Are you sure you want to remove this item from your cart?',
             buttons : {
                 Yes : {
                     btnClass: 'btn btn-primary',
                     action : function () {
                         window.location.href='{{route('cart-remove', ':id')}}'.replace(':id', id);
                     }
                 },
                 Cancel : function () {}
             }
         });
     });
    </script>
@endsection
