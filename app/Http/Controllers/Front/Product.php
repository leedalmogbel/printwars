<?php

namespace App\Http\Controllers\Front;

use Request;
use App\Http\Controllers\Controller;
use App\Product as ProductModel;
use Session;
use App\User;

class Product extends Controller
{
    //
    public function detail(ProductModel $product, $name) {
        if (!$product) {
            abort(404);
        }

        if ($product->dashedTitle != $name) {
            abort(404);
        }

        $data['product'] = $product;
        return view('front.product.detail', $data);
    }

    public function search () {
        $data = [];

        try {
            $data['products'] = ProductModel::searchByKeyword(Request::get('keyword'));
        } catch (\Exception $e) {
            $this->setToastrMsg($e->getMessage(), 'error');
            return redirect()->back();
        }

        return view('front.search', $data);
    }

    public function seller(User $user) {
        if ($user->type != 'seller') {
            abort(404);
        }

        $products = $user
            ->products()
            ->where('active', 1)
            ->where('status', 2)
            ->paginate(12);

        $data = [
            'products' => $products,
            'user' => $user
        ];

        return view('front.seller', $data);
    }
}
