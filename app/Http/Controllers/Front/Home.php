<?php

namespace App\Http\Controllers\Front;

use Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Repos\Invoice\PaymentMethod;

class Home extends Controller
{
    //
    public function home() {
        $data['discover'] = Product::where('active', 1)
            ->where('status', 2)
            ->orderBy('updated_at', 'desc')
            ->paginate(12);

        $data['bestSeller'] = Product::bestSeller()
            ->paginate(12);
        
        return view('front.home', $data);
    }
}
