<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Request;
use Session;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected function validateRedirectURL ($url) {
        if (!$url) {
            return route('home');
        }
        
        $protocol = 'http://';
        if (Request::secure()) {
            $protocol = 'https://';
        }

        if (strpos($url, $protocol) === 0 && strpos($url, $protocol . Request::getHttpHost()) !== 0) {
            throw new \Exception('Invalid redirect URL');
        }

        return $url;
    }

    protected function setToastrMsg($msg, $type = 'info') {
        // if request is ajax,
        if (request()->ajax()) {
            header('Content-Type: application/json');
            die(json_encode(['type' => $type,
                'msg' => $msg
            ]));
        }
        
        Session::flash('msg', $msg);
        Session::flash('msg_type', $type);

        return $this;
    }
}
