@extends('front.partials.frame')
@section('hero')
    @include('front.partials.hero')
@endsection

@section('content')
    <div class="container">
        <div class="product-slider">
            <div class="slider-title">Recommended for you</div>
            <div class="row slider-body">
            </div>
        </div>
        <hr />
        <div class="product-slider">
            <div class="slider-title">Best Seller</div>
            <div class="row slider-body">
                @foreach($bestSeller as $product)
                @include('front.partials.product.grid', ['product' => $product])
                @endforeach
            <br />
            </div>
        </div>
        <hr />
        <div class="product-slider">
            <div class="slider-title">Discover</div>
            <div class="row slider-body">
                @foreach($discover as $product)
                @include('front.partials.product.grid', ['product' => $product])
                @endforeach
            </div>
            <br />
            {{$discover->links()}}
        </div>
        <Br />
    </div>
@endsection
