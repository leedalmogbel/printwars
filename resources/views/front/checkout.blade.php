@extends('front.partials.frame')

@section('content')
    <br />
    <div class="container checkout-page">
        <br />
        <form method="post">
            @csrf
            <h1>Checkout</h1>
            <br />
            <div class="form row">
                <div class="col-md-7">
                    <div class="shipping-address">
                        <h4 class="text-primary">Shipping Info</h4>
                        <hr />
                        <div class="form-group">
                            <label>Email Address</label>
                            <input type="text"
                                   name="email"
                                   placeholder="Enter email address"
                                   value="{{old('email') ? old('email') : Session::get('me')->email}}"
                                   class="form-control{{$errors->has('email') ? ' is-invalid': ''}}" />
                            @if ($errors->has('email'))
                                @foreach ($errors->get('email') as $error)
                                    <small class="text-danger">{{$error}}</small>
                                @endforeach
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Full name</label>
                            <input type="text"
                                   name="name"
                                   placeholder="Full name"
                                   value="{{old('name') ? old('name') : Session::get('me')->name}}"
                                   class="form-control{{$errors->has('name') ? ' is-invalid':null}}" />
                            @if ($errors->has('name'))
                                @foreach ($errors->get('name') as $error)
                                    <small class="text-danger">{{$error}}</small>
                                @endforeach
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Contact number</label>
                            <input type="text"
                                   name="contact"
                                   placeholder="Enter contact number"
                                   value="{{old('contact')}}"
                                   class="form-control{{$errors->has('contact') ? ' is-invalid':null}}" />
                            @if ($errors->has('contact'))
                                @foreach ($errors->get('contact') as $error)
                                    <small class="text-danger">{{$error}}</small>
                                @endforeach
                            @endif
                        </div>
                        <p>
                            <strong class="text-primary">Shipping Address</strong>
                        </p>
                        <div class="form-group">
                            <input type="text"
                                   name="street"
                                   placeholder="Street address"
                                   value="{{old('street')}}"
                                   class="form-control{{$errors->has('street') ? ' is-invalid':null}}" />
                                   @if ($errors->has('street'))
                                       @foreach ($errors->get('street') as $error)
                                           <small class="text-danger">{{$error}}</small>
                                       @endforeach
                                   @endif
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <input type="text"
                                       name="city"
                                       value="{{old('city')}}"
                                       placeholder="City"
                                       class="form-control{{$errors->has('city') ? ' is-invalid':null}}" />
                                       @if ($errors->has('city'))
                                           @foreach ($errors->get('city') as $error)
                                               <small class="text-danger">{{$error}}</small>
                                           @endforeach
                                       @endif
                            </div>
                            <div class="col-md-6">
                                <input type="text"
                                       name="province"
                                       value="{{old('province')}}"
                                       placeholder="Province"
                                       class="form-control{{$errors->has('province') ? ' is-invalid':null}}" />
                                       @if ($errors->has('province'))
                                           @foreach ($errors->get('province') as $error)
                                               <small class="text-danger">{{$error}}</small>
                                           @endforeach
                                       @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <input type="text"
                                       name="postal"
                                       value="{{old('postal')}}"
                                       placeholder="Postal code"
                                       class="form-control{{$errors->has('postal') ? ' is-invalid':null}}" />
                                       @if ($errors->has('postal'))
                                           @foreach ($errors->get('postal') as $error)
                                               <small class="text-danger">{{$error}}</small>
                                           @endforeach
                                       @endif
                            </div>
                            <div class="col-md-6">
                                <select name="region" id="region" class="form-control{{$errors->has('region') ? ' is-invalid':null}}">
                                    <option disabled selected>Select a Region</option>
                                    <option value="ncr"{{old('region') == 'ncr' ? ' selected':null}}>National Capital Region</option>
                                    <option value="luz"{{old('region') == 'luz' ? ' selected':null}}>Luzon</option>
                                    <option value="vis"{{old('region') == 'vis' ? ' selected':null}}>Visayas</option>
                                    <option value="min"{{old('region') == 'min' ? ' selected':null}}>Mindanao</option>
                                </select>
                                @if ($errors->has('region'))
                                    @foreach ($errors->get('region') as $error)
                                        <small class="text-danger">{{$error}}</small>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="form-group">
                        <input type="text"
                               name="country"
                               placeholder="Country"
                               value="Philippines"
                               disabled="disabled"
                               class="form-control" />
                    </div>
                    <hr />
                    <br />
                    <div class="card payment-list">
                        <div class="card-header"><h4>Payment method</h4></div>
                        <div class="card-body">
                            <div class="payment-item">
                                <input type="radio"
                                       class="payment-radio"
                                       value="cod"
                                       @if(old('payment') == 'cod')
                                       checked
                                       @endif
                                       name="payment" />

                                Cash on delivery
                            </div>
                            @if ($errors->has('payment'))
                                @foreach ($errors->get('payment') as $error)
                                    <small class="text-danger">{{$error}}</small>
                                @endforeach
                            @endif
                        </div>
                    </div>
                    <br />
                    <button type="submit" class="btn btn-primary checkout-btn">
                        <span class="fa fa-shopping-cart"></span>
                        Submit Order
                    </button>
                    <br />
                </div>
                <div class="col-md-5 order-summary">
                    <a href="{{route('cart')}}" class="float-right">
                        <span class="fa fa-shopping-basket"></span> Modify cart
                    </a>
                    <h4 class="text-primary">Order Summary</h4>
                    @foreach ($cart as $quote)
                        <div class="row">
                            <div class="col-md-6">
                                <img src="{{$quote->product()->first()->image('front')}}" />
                                <div>{{$quote->product()->first()->title}}</div>
                                <div class="text-secondary">&mdash; {{$quote->actualSize()}}</div>
                            </div>
                            <div class="col-md-2">
                                <span class="text-secondary">x {{$quote->qty}}</span>
                            </div>
                            <div class="col-md-4 text-right">
                                PHP {{number_format($quote->amount, 2)}}
                            </div>
                        </div>
                    @endforeach
                    <hr />
                    <div class="row">
                        <div class="col text-secondary">
                            <div>Shipping Total</div>
                            <div>Order Total</div>
                            <div>GRAND TOTAL</div>
                        </div>
                        <div class="col">
                            <div class="text-right text-success">PHP {{number_format($cart->shippingAmount, 2)}}</div>
                            <div class="text-right text-success">PHP {{number_format($cart->totalAmount, 2)}}</div>
                            <div class="text-right text-success">PHP {{number_format($cart->totalAmount + $cart->shippingAmount, 2)}}</div>
                        </div>
                    </div>
                    <br />
                    <hr />
                    <p class="text-secondary">Your order will be delivered within 5-10 business days</p>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('custom-scripts')
    <script>
     $('#region').change(function () {
         let val = $(this).val();
         let url = '{{route('checkout-update-region', ':reg')}}'.replace(':reg', val);
         $('.checkout-page').css({'opacity' : '0.4'});
         $.get(url, function (res) {
             $('.checkout-page').css({'opacity' : '1'});
             try {
                 if (res.type == 'success') {
                     window.href.reload();
                     return;
                 }

                 if (res.type == 'error') {
                     toastr['error'](res.msg || 'Something went wrong');
                     return;
                 }

                 console.log(res);
                 let html = $(res).find('.order-summary').html();
                 $('.order-summary').html(html);
                 
             } catch (e) {
                 toastr['error']('Something went wrong');
             }
         }).fail(function() {
             $('.checkout-page').css({'opacity' : '1'});
             toastr['error']('Something went wrong');
         });
     });
    </script>
@endsection
