@extends('admin.partials.frame')

@section('content')
    <div class="card">
        <div class="card-header">Designs</div>
        <div class="card-body">
            <form>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Search Product Title or SKU</span>
                    </div>
                    <input type="text" class="form-control"
                           name="q"
                           placeholder="Search here"
                           value="{{ request()->get('q') }}" />
                    
                    <div class="input-group-append">
                        <button class="btn btn-success" type="submit"><span class="fa fa-search"></span> Search</button>
                    </div>
                </div>
            </form>
            <br />
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a href="{{ route('admin-product', 'all') }}" class="nav-link{{$status == 'all' ? ' active' : '' }}">All</a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin-product', 'pending') }}" class="nav-link{{$status == 'pending' ? ' active' : ''}}">Pending</a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin-product', 'approved') }}" class="nav-link{{$status == 'approved' ? ' active' : ''}}">Approved</a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin-product', 'rejected') }}" class="nav-link{{$status == 'rejected' ? ' active' : ''}}">Rejected</a>
                </li>
            </ul>
            <br />
            <table class="table table-striped table-bordered">
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>SKU</th>
                    <th>Preview</th>
                    <th>Price</th>
                    <th>Seller</th>
                    <th>Status</th>
                    <th>Created</th>
                    <th>Updated</th>
                    <th><center>Action</center></th>
                </tr>
                @foreach ($products as $product)
                    <tr>
                        <td>{{ $product->id }}</td>
                        <td>{{ $product->title }}</td>
                        <td>{{ $product->sku }}</td>
                        <td><img src="{{ $product->image('front') }}" class="img-list" />
                        <td>PHP {{ number_format($product->price, 2) }}
                        <td>
                            <a href="#">
                                {{ $product->seller()->first()->name }}
                            </a>
                        </td>
                        <td>
                            @if ($product->active == 1)
                                <span class="text-success">Active</span>
                            @else
                                <span class="text-danger">Inactive</span>
                            @endif
                    &mdash;
                            @if ($product->status == 1)
                                <span class="text-info">Pending</span>
                            @elseif ($product->status == 2)
                                <span class="text-success">Approved</span>
                            @else
                                <span class="text-danger">Rejected</span>
                            @endif
                        </td>
                        <td>{{ date('M d, Y H:i:s', strtotime($product->created_at)) }}</td>
                        <td>{{ date('M d, Y H:i:s', strtotime($product->updated_at)) }}</td>
                        <td>
                            <center>
                                <a href="{{ route('admin-product-detail', $product->id) }}" title="View"><span class="fa fa-eye text-info"></span></a>
                                @if ($product->status != 0)
                                    <a href="#" title="Reject" class="remove-product" data-id="{{ $product->id }}">
                                        <span class="fa fa-trash text-danger"></span>
                                    </a>
                                @endif
                                @if ($product->status !=2)
                                    <a href="#" title="Approve" class="activate-product" data-id="{{ $product->id }}">
                                        <span class="fa fa-check text-success"></span>
                                    </a>
                                @endif
                            </center>
                        </td>
                    </tr>
                @endforeach
            </table>
            <br />
            {{ $products->appends(Request::except('page'))->links() }}
        </div>
    </div>
@endsection

@section('custom-css')
    <link type="text/css" rel="stylesheet" href="/components/jquery-confirm/dist/jquery-confirm.min.css" />
@endsection
@section('custom-scripts')
    <script type="text/javascript" src="/components/jquery-confirm/dist/jquery-confirm.min.js"></script>
    <script>
     window.page = 'product';
     $('.remove-product').click(function (e) {
         e.preventDefault();
         let id = $(this).data('id');
         $.confirm({
             title : 'Reject Design',
             type: 'red',
             content : 'Are you sure you want to reject this design?',
             buttons : {
                 Yes:  {
                     btnClass : 'btn btn-danger',
                     action: function() {
                         window.location.href="{{ route('admin-product-reject', 'id') }}".replace('id', id);
                     }
                 },
                 Cancel: function() {

                 }
             }
         });
     });
     
     $('.activate-product').click(function (e) {
         e.preventDefault();
         let id = $(this).data('id');
         $.confirm({
             title : 'Approve Design',
             type: 'green',
             content : 'Are you sure you want to approve this design?',
             buttons : {
                 Yes: {
                     btnClass: 'btn btn-success',
                     action: function() {
                         window.location.href="{{ route('admin-product-approve', 'id') }}".replace('id', id);
                     }
                 },
                 Cancel: function() {

                 }
             }
         });
     });
    
    </script>
@endsection
