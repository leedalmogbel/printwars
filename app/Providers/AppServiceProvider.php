<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repos\Invoice\PaymentMethod;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        
    }
}
