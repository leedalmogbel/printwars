<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->json('shipping');
            $table->integer('user')->index();
            $table->string('email')->index();
            $table->string('name');
            $table->string('phone');
            $table->string('method')->index();
            $table->float('shipping_amount', 11, 2);
            $table->float('total_amount', 11, 2);
            $table->integer('status')->index();
            $table->string('reference')->index();
            $table->string('txnid')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
