@extends('seller.partials.frame')

@section('content')
    <div class="container-fluid">
        <div class="product-design">
            <h1>Create your own Design</h1>
            <hr />
            <div class="row">
                <div class="col-sm-4 design-controls">
                    <div>
                        <div class="select-color">
                            <h5>T Shirt Color</h5>
                            <div class="row colors"></div>
                        </div>
                        <br />
                        <hr />
                        <div class="txt">
                            <a href="#" class="btn btn-success float-right circle add-text front-control">
                                <small class="fa fa-plus"></small>
                            </a>
                            <a href="#" class="btn btn-success float-right circle add-text back-control">
                                <small class="fa fa-plus"></small>
                            </a>
                            <h5>Text Design</h5>
                            <div class="txtboxes front-control"></div>
                            <div class="txtboxes back-control"></div>
                        </div>
                        <br />
                        <hr />
                        <div class="design-img front-control">
                            <h5>Add Images</h5>
                            <div class="img-list"></div>
                        </div>
                        <div class="design-img back-control">
                            <h5>Add Images</h5>
                            <div class="img-list"></div>
                        </div>
                    </div>
                    <hr />
                    <br />
                    <button class="btn btn-success submit-design">Submit</button>
                </div>
                <div class="col-sm-8">
                    <div class="shirt-side-btn">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-outline-success front-btn" data-val="front">Front</button>
                            <button type="button" class="btn btn-outline-success back-btn" data-val="back">Back</button>
                        </div>
                    </div>
                    <div class="tshirt-design front-side front-control"></div>
                    <div class="tshirt-design back-side back-control"></div>
                </div>
            </div>
        </div>
        <div class="final">
            <h2>T-Shirt Information</h2>
            <hr />
            <div class="form-group">
                <label>T-Shirt Title</label>
                <input type="text"
                       name="title"
                       placeholder="Enter your t-shirt title"
                       class="form-control final-title" />
            </div>
            <div class="form-group">
                <label>T-Shirt Description</label>
                <textarea name="description"
                          placeholder="Enter description"
                          class="form-control final-description"></textarea>

            </div>
            <br />
            <div class="design-imgs row text-center">
                <div class="col"><img src="/images/tshirts/front.png" id="final-front" /></div>
                <div class="col"><img src="/images/tshirts/back.png" id="final-back" /></div>
            </div>
            <br />
            <hr />
            <br />
            <div class="text-center">
                <button type="submit" class="btn btn-secondary" id="cancel-final">&laquo; Back</button>
                <button type="submit" class="btn btn-success" id="submit-final">Submit T-Shirt</button>
            </div>
        </div>
    </div>
@endsection

@section('custom-css')
    <link type="text/css" rel="stylesheet" href="/components/jquery-ui-dist/jquery-ui.min.css" />
    <link type="text/css" rel="stylesheet" href="/components/jquery-ui-rotatable/jquery.ui.rotatable.css" />
    <link href="https://fonts.googleapis.com/css?family=Alfa+Slab+One|Amatic+SC|Anton|Bahianita|Catamaran|DM+Serif+Text|Dancing+Script|Francois+One|Indie+Flower|Kaushan+Script|Literata|Lobster|Noto+Sans+SC|Open+Sans|Pacifico|Permanent+Marker|Righteous|Roboto+Slab|Rubik+Mono+One|Russo+One|Satisfy|Shadows+Into+Light&display=swap" rel="stylesheet">
    <style>
     .product-design {
         margin: auto;
         margin-top: 10px;
         max-width: 1600px;
     }

     .colors { margin-top: 25px; padding: 5px 10px }
     .product-design h1 { text-align: center; }
     .tshirt-design { margin-top: 50px; }
     .design-controls {
         border-right: 1px solid #ddd;
         margin-top: -16px;
         padding-top: 24px;
         margin-bottom: 20px;
     }

     .design-controls hr {
         margin-right: -15px;
     }

     .txtboxes {
         margin: 20px -16px 0 0;
     }

     .circle { border-radius: 50%; }
     .ui-rotatable-handle {
         display: none;
         margin-bottom:-15px;
         margin-left: -15px;
         position: absolut;ed
     }

     .shirt-side-btn {
         text-align: center;
     }

     .design-controls > div { padding: 5px; }

     .design-controls ::-webkit-scrollbar {
         width: 5px;
         background-color: #F5F5F5;
     }

     .design-controls ::-webkit-scrollbar-thumb {
         background-color: #38c172;
     }
     
     .design-controls ::-webkit-scrollbar-track {
         -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
         background-color: #F5F5F5;
     }

     .final {
         max-width: 600px;
         margin: auto;
         display: none;
     }

     .final .final-description { height: 150px; }
     .final .design-imgs img { max-width: 250px; }
    </style>
@endsection

@section('custom-scripts')
    <script type="text/javascript" src="/components/jquery-ui-dist/jquery-ui.min.js"></script>
    <script src="/components/jquery-ui-rotatable/jquery.ui.rotatable.min.js"></script>
    <script>
     
     class Design {
         constructor (selector, options) {
             options = options || {};
             this.load(selector);
             this.selector = selector;
             this.color = '#ffffff';
             if (typeof options.color != 'undefined') {
                 this.color = options.color;
             }

         }

         load (selector) {
             let mainDiv = document.createElement('div');
             mainDiv.className = "main-design-div";

             $(selector).html(mainDiv);
         }
         
         loadImg() {
             let imgUrl = `/images/tshirts/${this.view}`;
             let imgDiv = document.createElement('div');
             imgDiv.className = 'image-div';
             imgDiv.style.cssText = 'text-align: center;'
                 + 'position: relative;'
                 + 'max-width: 700px;'
                 + 'margin: auto;'
                 + 'width: auto;'
                 + `background-color: ${this.color}`
                 + 'height: auto';

             let img = document.createElement('img');
             img.src = imgUrl;
             img.style.cssText = `width:100%; object-fit: contain; background-color: transparent`;
             imgDiv.appendChild(img);
             
             $(`${this.selector} .main-design-div`).html(imgDiv);
         }

         setColor(color) {
             $(`${this.selector} .image-div`).css({'background' : color});
         }

         getColor() {
             return $(`${this.selector} .image-div`).css('backgroundColor');
         }

         addText (text, styles) {
             text = text || '';
             styles = styles || {};
             styles.position = 'absolute';
             styles.cursor = 'move';
             styles.outline = 'none';
             styles.fontSize = '10px';
             styles.textAlign = 'left';
             styles.border = '1px solid transparent';
             styles.whiteSpace = 'nowrap';
             
             let txt = document.createElement('div');
             txt.className = 'design-txt';
             
             txt.setAttribute("tabIndex", "0");
             
             $(txt).css(styles);
             let span = document.createElement('span');
             txt.appendChild(span);
             span.innerHTML = text;
             $(this.selector).find('.design-square').append(txt);
             $(txt).draggable();
             
             var initDiagonal;
             var initFontSize;
             
             /*$(txt).resizable({
                 'containment' : 'parent',
                 create: function(event, ui) {
                     let w = $(this).width();
                     let h = $(this).height();
                     initDiagonal = w *  w + h *h;
                     initFontSize = parseInt($(txt).css("font-size"));
                 },
                 resize: function(e, ui) {
                     let w = $(this).width();
                     let h = $(this).height();
                     let newDiagonal = w *  w + h *h;
                     var ratio = newDiagonal / initDiagonal;

                     let newFontSize = initFontSize + ratio * 2;
                     $(this).css({"font-size" : newFontSize, "line-height" : `${(parseInt(newFontSize) + 4)}px`});
                 }
             });*/

             $(txt).rotatable();
             $(txt).find('.ui-resizable-handle').hide();
             $(txt).focus(function () {
                 $('.design-txt').css('border', '1px solid transparent');
                 $(this).css('border', '1px solid red');
                 $(this).find('.ui-rotatable-handle').show();
             }).mousedown(function () {
                 $(this).focus();
             }).blur(function () {
                 $(this).css('border', '1px solid transparent');
                 $(txt).find('.ui-rotatable-handle').hide();
             });

             return txt;
         }

         addImage (src) {
             let div = document.createElement('div');
             let img = document.createElement('img');

             div.setAttribute("tabIndex", "0");
             img.style.cssText = 'max-width: 100%;max-height: 100%;';
             img.src = src;
             div.style.cssText = 'position: absolute;width: 100px; height: 100px;';
             div.appendChild(img);
             $(this.selector).find('.design-square').append(div);
             
             $(div).draggable();
             $(div).rotatable();
             $(div).resizable();

             $(div).focus(function () {
                 $(this).css('border', '1px solid red');
                 $(this).find('.ui-rotatable-handle').show();
                 $(this).find('.ui-resizable-handle').show();
             }).mousedown(function () {
                 $(this).focus();
             }).blur(function () {
                 $(this).css('border', '1px solid transparent');
                 $(this).find('.ui-rotatable-handle').hide();
                 $(this).find('.ui-resizable-handle').hide();
             });

             $(div).find('.ui-rotatable-handle').hide();
             $(div).find('.ui-resizable-handle').hide();
             return div;
         }

         getImages () {
             let imgs = [];
             $(`${this.selector} .design-square img`).each(function() {
                 imgs.push($(this).attr('src'));
             });

             return imgs;
         }

         getText() {
             let txt = [];
             $(`${this.selector} .design-square .design-txt span`).each(function() {
                 txt.push({
                     txt : $(this).html(),
                     font : window.getComputedStyle(this, null).getPropertyValue('font-family')
                 });
             });

             return txt;
         }
     }

     class Front extends Design {
         constructor (selector, options) {
             super(selector, options);
             this.view = 'front.png';
             this.loadImg();
             
             this.loadSquare();
         }

         loadSquare () {
             let div =  document.createElement('div');
             div.className = 'design-square';
             div.style.cssText = 'position: absolute;'
                 + 'left: 50%;'
//                 + 'border: 1px solid red;'
                 + 'margin-left: -20%;'
                 + 'top: 20%;'
                 + 'overflow: hidden;'
                 + 'width: 40%; height: 47%;';

             $(`${this.selector} .image-div`).prepend(div);
         }
     }
     
     class Back extends Design {
         constructor (selector, options) {
             super(selector, options);
             this.view = 'back.png';
             this.loadImg();
             
             this.loadSquare();
         }

         loadSquare () {
             let div =  document.createElement('div');
             div.className = 'design-square';
             div.style.cssText = 'position: absolute;'
                 + 'left: 50%;'
                 //+ 'border: 1px solid red;'
                 + 'margin-left: -20%;'
                 + 'top: 20%;'
                 + 'overflow: hidden;'
                 + 'width: 40%; height: 46%;';

             $(`${this.selector} .image-div`).prepend(div);
         }
     }

     class Colors {
         constructor (selector, colors) {
             this.selector = selector;
             this.colors = colors;
             this.load();
         }

         load () {
             for (let i = 0; i < this.colors.length; i++) {
                 this.loadElm(this.colors[i]);
             }
         }

         loadElm(color) {
             let elm = document.createElement('a');
             elm.href='#';
             elm.style.cssText = 'border: 1px solid #555;'
                 + `background : ${color};`
                 + 'display : block;'
                 + 'width : 20px;'
                 + 'height : 20px;'
                 + 'margin : auto;'
                 + 'border-radius: 50%;'
                 + 'text-decoration : none;';

             elm.title = color.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                 return letter.toUpperCase();
             });

             let pElm = document.createElement('div');
             pElm.className = 'col';
             pElm.style.cssText = 'margin-top: 10px;'
             pElm.appendChild(elm);

             $(this.selector).append(elm);
         }

         on (ev, callback) {
             $(document).on(ev, `${this.selector} a`, function(e) {
                 callback.call(this, e);
             });
         }
     }

     class Text {
         constructor (selector) {
             this.selector = selector;

             $(document).on('click', '.txt-ctrl', function () {
                 if (!$(this).hasClass('active')) {
                     $(this).addClass('active');
                     $(this).data('value', 'true');
                     $(this).parent().parent().parent().change();
                     return;
                 }

                 $(this).data('value', 'false');
                 $(this).removeClass('active');
                 $(this).parent().parent().parent().change();
             });

             $(document).on('click', '.rm', function () {
                 $(this).addClass('active');
                 $(this).parent().parent().parent().change();
             });
         }

         loadFonts (fonts) {
             this.fonts = fonts;
         }

         addNew (design) {
             let txt = 'new text';
             let self = this;
             let div = document.createElement('div');
             div.className = 'row';
             div.style.cssText = 'padding: 15px 0 5px 0; margin: 10px 0 0 0; border-top: 1px solid #f1f1f1';
             let col1 = document.createElement('div');
             let col2 = document.createElement('div');
             col1.className = 'col';
             col2.className = 'col';
             
             let txtbox = document.createElement('textarea');
             txtbox.className = 'form-control';
             txtbox.style.cssText = 'height: 135px; border: 1px solid #ccc; border-radius: 2px;'
             txtbox.innerHTML = txt;

             let colorDiv = document.createElement('div');
             let color = document.createElement('input');
             let bold = document.createElement('button');
             bold.innerHTML = 'B';
             bold.className = 'btn btn-info txt-ctrl txt-b';
             bold.style.cssText = 'font-family: bold; margin-left: 5px; font-family: serif';
             bold.setAttribute('data-value', 'false');
             
             let italic = document.createElement('button');
             italic.innerHTML = 'I';
             italic.className = 'btn btn-info txt-ctrl txt-i';
             italic.style.cssText = 'font-style: italic; margin-left: 5px;font-family: serif';
             italic.setAttribute('data-value', 'false');

             let underline = document.createElement('button');
             underline.innerHTML = 'U';
             underline.className = 'btn btn-info txt-ctrl txt-u';
             underline.style.cssText = 'text-decoration: underline; margin-left: 5px;font-family: serif';
             underline.setAttribute('data-value', 'false');

             let rm = document.createElement('button');
             let rmSpan = document.createElement('span');
             rmSpan.className = 'fa fa-trash';
             rm.className = 'btn btn-danger rm';
             rm.style.cssText = 'margin-left: 10px;';

             rm.appendChild(rmSpan);
             let fontSizeDiv = document.createElement('div');
             let fontSize = document.createElement('div');
             let fontSizeValue = document.createElement('input');
             fontSizeValue.setAttribute('type', 'hidden');
             fontSizeValue.value = '10';
             fontSizeValue.className = 'font-size';
             
             fontSize.className = 'slider';
             fontSizeDiv.style.cssText = 'margin-left: -10px;'
             fontSizeDiv.appendChild(fontSize);
             fontSizeDiv.appendChild(fontSizeValue);
             
             color.setAttribute('type', 'color');
             color.className = 'txt-color-picker';
             color.style.cssText = 'height: 37px;';
             colorDiv.className = 'row';
             
             colorDiv.appendChild(color);
             colorDiv.appendChild(bold);
             colorDiv.appendChild(italic);
             colorDiv.appendChild(underline);
             colorDiv.appendChild(rm);
             
             let d = design.addText(txt);
             
             let fontFamilyDiv = document.createElement('div');
             fontFamilyDiv.className = 'row';
             let fontFamily = document.createElement('select')
             fontFamily.className = 'font-family-select form-control';
             fontFamily.style.cssText = 'width: 93%'
             let option = document.createElement('option');
             option.innerHTML = 'Select Font'
             option.setAttribute('disabled', true);
             option.setAttribute('selected', true);
             fontFamily.appendChild(option);
             
             for(let fontName in this.fonts) {
                 let option = document.createElement('option');
                 option.innerHTML = fontName;
                 option.value = this.fonts[fontName];
                 option.style.cssText = `font-family:${this.fonts[fontName]}`;
                 fontFamily.appendChild(option);
             }

             if (Object.keys(this.fonts).length > 2) {
                 fontFamily.setAttribute('size', 2);
             }

             fontFamilyDiv.appendChild(fontFamily);

             col1.appendChild(txtbox);
             col2.appendChild(colorDiv);
             col2.appendChild(fontSizeDiv);
             col2.appendChild(fontFamilyDiv)
             
             div.appendChild(col1);
             div.appendChild(col2);
             $(this.selector).append(div);

             $(fontSize).slider({
                 slide: function (e, ui) {
                     $(this).parent().find('.font-size').val(ui.value);
                     $(div).change();
                 }
             });

             $(col2).children().css({'margin-bottom' : '10px'});
             
             //$(fontFamily).selectmenu();
             
             $(div).change(function() {
                 return self.textToDesign(this, d);
             });
         }

         textToDesign(txtbox, d) {
             if ($(txtbox).find('.rm').hasClass('active')) {
                 $(txtbox).remove();
                 $(d).remove();
                 return;
             };
             
             let txt = $(txtbox).find('textarea').val();
             $(d).find('span').html(txt);

             let styles = {};
             styles.color = $(txtbox).find('.txt-color-picker').val();

             let fontSize = $(txtbox).find('.font-size').val();
             if (fontSize < 10) {
                 fontSize = 10;
             }
             
             styles.fontWeight = 'normal';
             styles.fontStyle = 'normal';
             styles.textDecoration = 'none';
             
             if ($(txtbox).find('.txt-b').hasClass('active')) {
                 styles.fontWeight = 'bold';
             }
             
             if ($(txtbox).find('.txt-i').hasClass('active')) {
                 styles.fontStyle = 'italic';
             }
             
             if ($(txtbox).find('.txt-u').hasClass('active')) {
                 styles.textDecoration = 'underline';
             }
             
             styles.fontSize = `${fontSize}px`;
             styles.lineHeight = `${fontSize}px`;
             
             let fontFamily = $(txtbox).find('.font-family-select').val();
             if (fontFamily) {
                 styles.fontFamily = fontFamily;
             }

             $(d).find('span').css(styles);
         }
     }

     class Img {
         constructor(selector, design) {
             this.selector = selector;
             this.design = design;
             this.loadInput();
         }

         loadInput () {
             let input = document.createElement('input');
             input.type="file";
             input.style.cssText = 'display: none;';
             $(this.selector).append(input);
             this.inputFile = input;
             let inputSelect = document.createElement('a');
             inputSelect.href = '#';
             inputSelect.style.cssText = 'display: inline-block; margin-top: 15px';
             inputSelect.className = 'btn btn-success';
             
             inputSelect.innerHTML = 'Upload an image';
             this.inputSelect = inputSelect;
             $(this.selector).append(inputSelect);

             $(inputSelect).click(function (e) {
                 e.preventDefault();
                 $(input).click();
             });

             let self = this;
             $(input).change(function () {
                 if (typeof this.files[0] != 'undefined' && typeof this.files != 'undefined') {
                     let reader = new FileReader();

                     reader.onload = function (e) {
                         let div = document.createElement('div');
                         div.style.cssText = 'padding: 10px; border-bottom: 1px dashed #ddd';
                         let img = document.createElement('img');
                         img.style.cssText = 'max-width: 100px; max-height: 100px;border: 1px solid #eee; padding: 5px';
                         img.src = e.target.result;
                         let a = document.createElement('a');
                         a.href = '#';
                         a.innerHTML = 'REMOVE IMAGE';
                         a.className = 'btn btn-danger remove-image';
                         a.style.cssText = 'float: right; font-size: 11px';
                         
                         div.appendChild(a);
                         div.appendChild(img);
                         
                         $(`${self.selector} .img-list`).append(div);

                         
                         let imgDesign = self.design.addImage(e.target.result);

                         $(a).click(function (e) {
                             e.preventDefault();
                             $(this).parent().remove();
                             $(imgDesign).remove();
                         });
                         
                         $(input).val('');
                     }

                     reader.readAsDataURL(this.files[0]);

                 }
             });
         }
     }

     let fonts = {
         'Open Sans' : "'Open Sans', sans-serif",
         'Literata' : "'Literata', serif",
         'DM Serif Text' : "'DM Serif Text', serif",
         'Roboto Slab' : "'Roboto Slab', serif",
         'Bahianita' : "'Bahianita', cursive",
         'Russo One' : "'Russo One', sans-serif",
         'Rubik Mono One' : "'Rubik Mono One', sans-serif",
         'Anton' : "'Anton', sans-serif",
         'Indie Flower' : "'Indie Flower', cursive",
         'Lobster' : "'Lobster', cursive",
         'Pacifico' : "'Pacifico', cursive",
         'Dancing Script' : "'Dancing Script', cursive",
         'Shadows Into Light' : "'Shadows Into Light', cursive",
         'Catamaran' : "'Catamaran', sans-serif",
         'Righteous' : "'Righteous', cursive",
         'Amatic SC' : "'Amatic SC', cursive",
         'Satisfy' : "'Satisfy', cursive",
         'Francois One' : "'Francois One', sans-serif",
         'Permanent Marker' : "'Permanent Marker', cursive",
         'Nato Sans SC' : "'Noto Sans SC', sans-serif",
         'Alfa Slab One': "'Alfa Slab One', cursive",
         'Kaushan Script' : "'Kaushan Script', cursive",
     };
     
     // FRONT IMPLEMENTATION
     let front = new Front('.front-side', { color: 'white' });
     let fimg = new Img('.design-img.front-control', front);
     
     let ftxtbox = new Text('.txtboxes.front-control'); 
     ftxtbox.loadFonts(fonts);
     
     
     $('.front-control.add-text').click(function() {
         ftxtbox.addNew(front);
     });

     // BACK IMPLEMENTATION
     let back = new Back('.back-side', { color: 'white' });
     let bimg = new Img('.design-img.back-control', back);
     
     let btxtbox = new Text('.txtboxes.back-control'); 
     btxtbox.loadFonts(fonts);

     $('.back-control.add-text').click(function() {
         btxtbox.addNew(back);
     });

     
     
     let c = new Colors('.select-color .colors', [
         'white', 'black', 'gray',
         'blue', 'red', 'green',
         'yellow', 'orange', 'purple',
         'pink', 'maroon', 'brown'
     ]);
     
     c.on('click', function (e) {
         e.preventDefault();
         front.setColor(this.title.toLowerCase());
         back.setColor(this.title.toLowerCase());
     });

     // misc
     $('.back-control').hide();
     $('.front-btn').addClass('active');

     $('.shirt-side-btn button').click(function(e) {
         e.preventDefault();
         $('.shirt-side-btn button').removeClass('active');
         $(this).addClass('active');
         let val = $(this).data('val');
         $('.front-control, .back-control').hide();
         $(`.${val}-control`).show();
     });


     // page js
     window.hideSideBar();
     window.page = 'product';

     $('.submit-design').click(async function() {
         let params = {};
         params.frontImg = front.getImages();
         params.backImg = back.getImages();

         params.frontTxt = front.getText();
         params.backTxt = back.getText();

         $('.image-div > img').css({'position' : 'relative'});
         $('.front-side, .back-side').show();
         let w = $('.front-side .main-design-div .image-div').width();
         let h = $('.front-side .main-design-div .image-div').height();
         window.scrollTo(0,0)
         await html2canvas(document.querySelector('.front-side .main-design-div .image-div'),
             {height: (h + 50), width: (w + 50) })
             .then(function(canvas) {
                 params.frontSide = canvas.toDataURL();
         });
         
         w = $('.back-side .main-design-div .image-div').width();
         h = $('.back-side .main-design-div .image-div').height();
         await html2canvas(document.querySelector('.back-side .main-design-div .image-div'),
             { height: (w + 50), width: (w + 50), }).then(function(canvas) {
                 params.backSide = canvas.toDataURL();
         });

         $('.image-div > img').css({'position' : 'unset'});
         $('.back-control').hide();
         $('.front-control').show();
         $('.front-btn').addClass('active');
         window._params = params;

         $('#final-front').attr('src', window._params.frontSide);
         $('#final-back').attr('src', window._params.backSide);
         $('.product-design').fadeOut(100);
         $('.final').fadeIn(100);
     });

     $('#cancel-final').click(function() {
         $('.product-design').fadeIn(100);
         $('.final').fadeOut(100);
     });

     $('#submit-final').click(function() {
         $('.final').css({'opacity' : '0.3'});
         $('.final-title').removeClass('is-invalid');
         $('.final-description').removeClass('is-invalid');
         let params = window._params;
         params.title = $('.final-title').val();
         params.description = $('.final-description').val();

         if (params.title == '') {
             $('.final-title').addClass('is-invalid');
             $('.final').css({'opacity' : '1'});
             return;
         }
         
         if (params.description == '') {
             $('.final-description').addClass('is-invalid');
             $('.final').css({'opacity' : '1'});
             return;
         }

         params._token = "{{ csrf_token() }}";
         $.post('{{ route('seller-product-save') }}', params, function (res) {
             toastr['error']('Something went wrong. Please try again');
             $('.final').css({'opacity' : '1'});
             if (typeof res.errors != 'undefined') {
                 let errors = [];
                 for(let k in res.errors) {
                     toastr['error']('Something went wrong. Please try again');
                 }

                 return;
             }

             if (res.type != 'success') {
                 toastr['error'](res.message || 'Something went wrong. Please try again');
                 return;
             }

             window.location.href = '{{route('seller-product')}}';
             
         }).fail(function (xhr, status, e) {
             let res = xhr.responseJSON;
             if (typeof res.errors != 'undefined') {
                 for(let name in res.errors) {
                     $(`.final-${name}`).addClass('is-invalid');
                 }

             }

             toastr['error']('Something went wrong. Please try again');
             $('.final').css({'opacity' : '1'});
         });
     });

     $(document).ready(function() {
         $('.design-controls > div').css({
             'height' : parseInt($(document).height()) - parseInt($('.design-controls').offset().top) - 200,
             'overflow-x' : 'hidden'
         });
     });
    </script>
@endsection
