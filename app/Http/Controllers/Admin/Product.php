<?php

namespace App\Http\Controllers\Admin;

use Request;
use Session;
use App\Http\Controllers\Controller;

use App\Product as ProductModel;

class Product extends Controller
{
    //
    public function index ($status) {
        $data = [];
        $data['products'] = ProductModel::getProductsByStatus($status);
        $data['status'] = $status;
        
        return view('admin.product', $data);
    }

    public function approve (ProductModel $product) {
        try {
            $product->setStatus(2);
            $this->setToastrMsg('Status updated', 'success');

            return redirect()->back();
        } catch (\Exception $e) {
            $this->setToastrMsg($e->getMessage(), 'error');

            return redirect()->back();
        }
    }

    
    public function reject (ProductModel $product) {
        try {
            $product->setStatus(0);
            $this->setToastrMsg('Status updated', 'success');

            return redirect()->back();
            
        } catch (\Exception $e) {
            $this->setToastrMsg($e->getMessage(), 'error');

            return redirect()->back();
        }
    }

    public function detail (ProductModel $product) {
        if (empty($product)) {
            $this->setToastrMsg('Product not found', 'error');
            return redirect()->back();
        }
        
        return view('admin.product.detail', ['product' => $product]);
    }

    public function download() {
        if (!Request::get('path')) {
            $this->setToastrMsg('Invalid request', 'error');
            return redirect()->back();
        }
        
        if (!file_exists(public_path() . Request::get('path'))) {
            $this->setToastrMsg('File not found', 'error');
            return redirect()->back();
        }

        $file = Request::get('path');

        $quoted = sprintf('"%s"', addcslashes(basename(public_path() . $file), '"\\'));
        $size   = filesize(public_path() . $file);

        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . $quoted); 
        header('Content-Transfer-Encoding: binary');
        header('Connection: Keep-Alive');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . $size);
        die(file_get_contents(public_path() . $file));
    }

}
