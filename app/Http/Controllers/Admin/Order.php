<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Quote;
use App\Repos\Quote\Status;

class Order extends Controller
{
    //
    public function index($status) {
        try {
            $order = Quote::getQuoteWithInvoice($status);
        } catch (\Exception $e) {
            $this->setToastrMsg($e->getMessage());
            return redirect()->back();
        }

        return view('admin.orders', [
            'orders' => $order,
            'status' => $status,
        ]);
    }

    public function move(Quote $quote, $status) {
        try {
            $quote->status = Quote::statuses()->getValueByKey($status);
            $quote->save();

            $this->setToastrMsg('Package status updated', 'success');
            return redirect()->back();
        } catch (\Exception $e) {
            $this->setToastrMsg($e->getMessage(), 'error');
            return redirect()->back();
        }
    }

    public function statuses($key) {
        return Quote::statuses()->getNameByKey($key);
    }
}
