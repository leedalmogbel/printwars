<?php

namespace App\Events\Quote;

use App\Quote as QuoteModel;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class Updating
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $quote;
    
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(QuoteModel $quote)
    {
        $this->quote = $quote;
    }
}
