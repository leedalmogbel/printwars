<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Front route
Route::get('/', 'Front\Home@home')->name('home');
Route::get('/seller/home/{user}', 'Front\Product@seller')->name('seller-home');
Route::get('/product/detail/{product}/{name}', 'Front\Product@detail')->name('product-detail');
Route::get('/search', 'Front\Product@search')->name('product-search');
Route::get('/cart', 'Front\Cart@index')->name('cart');
Route::get('/cart/add', 'Front\Cart@add')->name('cart-add');
Route::get('/cart/update/{quote}', 'Front\Cart@update')->name('cart-update');
Route::get('/cart/remove/{quote}', 'Front\Cart@remove')->name('cart-remove');
Route::get('/checkout', 'Front\Invoice@checkout')->name('checkout');
Route::post('/checkout', 'Front\Invoice@order');
Route::get('/checkout/region/{region}', 'Front\Invoice@updateRegion')->name('checkout-update-region');
Route::get('/order', 'Front\Invoice@orders')->name('orders');
Route::get('/order/{invoice}', 'Front\Invoice@invoiceOrder')->name('checkout-order');
Route::get('/order/cancel/{invoice}', 'Front\Invoice@cancel')->name('order-cancel');

Route::get('/login', 'Front\User@login')->name('front-login');
Route::post('/login', 'Front\User@signin')->name('front-signin');
Route::get('/signup', 'Front\User@signup')->name('front-signup');
Route::post('/signup', 'Front\User@register')->name('front-register');


Route::get('/seller/login', 'Seller\User@login')->name('seller-login');
Route::post('/seller/login', 'Seller\User@signin')->name('seller-signin');

// Seller Routes
Route::group(['prefix' => 'seller', 'middleware' => 'login:seller'], function () {
    Route::get('dash', 'Seller\Dash@index')->name('seller-dash');
    Route::get('product', 'Seller\Product@index')->name('seller-product');
    Route::get('product/remove/{id}', 'Seller\Product@remove')->name('seller-product-remove');
    Route::get('product/detail/{id}', 'Seller\Product@detail')->name('seller-product-detail');
    Route::get('product/activate/{id}', 'Seller\Product@activate')->name('seller-product-activate');
    Route::get('product/create', 'Seller\Product@create')->name('seller-product-create');
    Route::get('product/design', 'Seller\Product@design')->name('seller-product-design');
    Route::post('product/save', 'Seller\Product@store')->name('seller-product-save');
    Route::get('order/{status}', 'Seller\Order@list')->name('seller-order-list');
    Route::get('order/cancel/{quote}', 'Seller\Order@cancel')->name('seller-order-cancel');
    Route::get('order/pay/{quote}', 'Seller\Order@payNow')->name('seller-order-paynow');

    Route::get('payment/success/{quote}', 'Seller\Payment@successQuote')->name('seller-payment-success');
    Route::get('payment/cancel/{quote}', 'Seller\Payment@cancelQuote')->name('seller-payment-cancel');

    Route::get('order/bulk/pay', 'Seller\Order@bulkPayment')->name('seller-bulk-pay');

    Route::get('payment-bulk/success/{payment}', 'Seller\Payment@success')->name('seller-payment-bulk-success');
    Route::get('payment-bulk/cancel/{payment}', 'Seller\Payment@cancel')->name('seller-payment-bulk-cancel');
});

Route::get('/admin/login', 'Admin\User@login')->name('admin-login');
Route::post('/admin/login', 'Admin\User@signin')->name('admin-signin');

// Admin Routes
Route::group(['prefix' => 'admin', 'middleware' => 'login:admin'], function () {
    Route::get('dash', 'Admin\Dash@index')->name('admin-dash');
    Route::get('product/{status}', 'Admin\Product@index')->name('admin-product');
    Route::get('product/approve/{product}', 'Admin\Product@approve')->name('admin-product-approve');
    Route::get('product/reject/{product}', 'Admin\Product@reject')->name('admin-product-reject');
    Route::get('product/detail/{product}', 'Admin\Product@detail')->name('admin-product-detail');
    Route::get('design/download', 'Admin\Product@download')->name('admin-design-download');

    Route::get('order/{status}', 'Admin\Order@index')->name('admin-order-list');
    Route::get('order/move/{quote}/{status}', 'Admin\Order@move')->name('admin-order-move');
    Route::get('quote/statuses/{key}', 'Admin\Order@statuses')->name('admin-quote-statuses');
    
    Route::get('seller/list', 'Admin\User@sellerList')->name('admin-seller-list');
    Route::get('seller/disable/{user}', 'Admin\User@sellerDisable')->name('admin-seller-disable');
    Route::get('seller/activate/{user}', 'Admin\User@sellerActivate')->name('admin-seller-activate');
});

Route::get('logout', function () {
    Session::flash('msg', 'Logout successfully');
    Session::flash('msg_type', 'info');
    Session::forget('me');
    
    return redirect('/');
})->name('logout');
//require_once (__DIR__ . '/front.php');
