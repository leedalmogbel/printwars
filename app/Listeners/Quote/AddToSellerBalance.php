<?php

namespace App\Listeners\Quote;

use App\Events\Quote\Updated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Wallet;
use App\Quote;


class AddToSellerBalance
{
    const DESCRIPTION = 'Payment for quote #%s, Order #%s';
    
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  QuoteUpdated  $event
     * @return void
     */
    public function handle(Updated $event)
    {
        if ($event->quote->status == 5) {
            $this->createWalletTransaction($event->quote);
        }
    }

    /**
     * create wallet transaction by quote
     *
     * @param Quote $quote
     * @return void
     */
    protected function createWalletTransaction($quote) {
        // get seller object
        // base on quote model
        $user = $quote
            ->product()
            ->first()
            ->seller()
            ->first();

        // get user balance
        $balance = $user->getBalance() + $quote->amount;

        // create wallet transaction
        Wallet::create([
            'user' => $user->id,
            'balance' => $balance,
            'amount' => $quote->amount,
            'transaction_description' => sprintf(self::DESCRIPTION, $quote->id, $quote->invoice),
            'type' => 'quote',
            'parent' => $quote->id
        ]);
        
    }
}
