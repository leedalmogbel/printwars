@extends('front.partials.frame')

@section('content')
    <br />
    <div class="search-page container">
        <br />
        <h1> Search result for <span class="text-primary">{{Request::get('keyword')}}</span></h1>
        <div class="text-secondary">1-12 out of 190 results</div>
        <hr />
        <div class="product-slider">
            <div class="row slider-body">
                @foreach($products as $product)
                @include('front.partials.product.grid', ['product' => $product])
                @endforeach
            </div>
        </div>
        <br />
        {{$products->appends(Request::except('page'))->links()}}
    </div>
@endsection
