<?php

namespace App\Http\Controllers\Seller;

use Request;
use Session;
use App\Product;
use App\User;
use App\Quote;
use App\Http\Controllers\Controller;

class Dash extends Controller
{
    //
    public function index () {
        $data = [];
        
        $data['pending'] = Quote::getOrdersBySeller(Session::get('me'), 'payable')
            ->count();
        
        $data['delivered'] = Quote::getDeliveredOrderBySellerThisMonth(Session::get('me'));
        $data['designs'] = Session::get('me')->getProducts(5);
        $data['tier'] = Session::get('me')->tier();
        
        return view('seller.dashboard', $data);
    }
}
