<?php
namespace App\Repos\Quote;

class Status
{
    private $data = [
        [
            'name' => 'Cancelled',
            'key' => 'cancelled',
            'value' => 0,
        ], [
            'name' => 'Needs Payment',
            'key' => 'payable',
            'value' => 1,
        ], [
            'name' => 'Payed',
            'key' => 'payed',
            'value' => 2,
        ], [
            'name' => 'For Shipping',
            'key' => 'for-shipping',
            'value' => 3,
        ], [
            'name' => 'Shipped',
            'key' => 'shipped',
            'value' => 4,
        ], [
            'name' => 'Delivered',
            'key' => 'delivered',
            'value' => 5,
        ]
    ];

    /**
     * returns all quote statuses
     *
     * @return array
     */
    public function all() {
        return $this->data;
    }

    /**
     * returns status name by key
     *
     * @param string $key
     * @return strng
     */
    public function getNameByKey($key) {
        return $this->getDataByKey($key)['name'];
    }

    /**
     * return status name by value
     *
     * @param numeric $value
     * @return string
     */
    public function getNameByValue($value) {
        return $this->getDataByValue($value)['name'];
    }

    /**
     * returns status key by value
     *
     * @param numeric $value
     * @return string
     */
    public function getKeyByValue($value) {
        return $this->getDataByValue($value)['key'];

    }

    /**
     * get status value by key
     *
     * @param string $key
     * @return string
     */
    public function getValueByKey($key) {
        return $this->getDataByKey($key)['value'];

    }

    /**
     * get status data by value
     *
     * @param numeric $value
     * @return array
     */
    public function getDataByValue($value) {
        if (!isset($this->data[$value])) {
            throw new \Exception('Status doesn\'t exist');
        }
        
        return $this->data[$value];
    }

    /**
     * get status data by key
     *
     * @param string $key
     * @return array
     */
    public function getDataByKey($key) {
        $data = $this->setKeyAsKey();
        if (!isset($data[$key])) {
            throw new \Exception('Status doesn\'t exist');
        }

        return $data[$key];
    }

    /**
     * process data to set key as key
     *
     * @return array
     */
    private function setKeyAsKey() {
        $newData = [];
        foreach ($this->data as $v) {
            $newData[$v['key']] = $v;
        }

        return $newData;
    }

    /**
     * init class
     *
     * @return App\Repos\Quote\Status
     */
    private static function init() {
        return new Status;
    }
}