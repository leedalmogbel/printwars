@extends('seller.partials.frame')
@section('content')
    <div class="wrapper">
        <h1>DASHBOARD</h1>
        <hr />
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">Your latest designs</div>
                    <div class="card-body">
                        <div>
                            <a class="btn btn-success" href="{{route('seller-product-design')}}">
                                <span class="fa fa-plus"></span> Create a design
                            </a>
                            <a class="btn btn-outline-success" href="{{route('seller-product')}}">
                                <span class="fa fa-tshirt"></span> All Designs
                            </a>
                        </div>
                        <br />
                        <table class="table table-striped table-bordered">
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Status</th>
                                <th>Created</th>
                                <th>Updated</th>
                            </tr>
                             @foreach ($designs as $product)
                                 <tr>
                                     <td>{{ $product->id }}</td>
                                     <td>{{ $product->title }}</td>
                                     <td>
                                     @if ($product->active == 1)
                                         <span class="text-success">Active</span>
                                     @else
                                         <span class="text-danger">Inactive</span>
                                     @endif
                                     &mdash;
                                     @if ($product->status == 1)
                                         <span class="text-info">Pending</span>
                                     @elseif ($product->status == 2)
                                         <span class="text-success">Approved</span>
                                     @else
                                         <span class="text-danger">Rejected</span>
                                     @endif
                                     </td>
                                     <td>{{ date('M d, Y H:i:s', strtotime($product->created_at)) }}</td>
                                     <td>{{ date('M d, Y H:i:s', strtotime($product->updated_at)) }}</td>
                                 </tr>
                             @endforeach
                        </table>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card">
                    <div class="card-header">Best seller</div>
                    <div class="card-body">
                        Lorem ipsum dolor set amit
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card">
                    <div class="card-header">Your Balance</div>
                    <div class="card-body">
                        <h3 class="text-primary">PHP {{number_format(Session::get('me')->getBalance(), 2)}}</h3>
                        <hr />
                        <div><a href="{{route('seller-order-list', 'payable')}}" class="text-danger">You have {{$pending}} unpayed order.</a></div>
                        <div><a href="{{route('seller-order-list', 'delivered')}}" class="text-success">You have {{$delivered}} delivered order for this month.</a></div>
                        <div><span class="text-primary">You are currently in {{$tier->getTierName()}}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom-scripts')
    <script>
     document.body.style.fontFeatureSettings = '"liga" 0';
     window.page = 'dashboard';
    </script>
@endsection
