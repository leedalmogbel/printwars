<?php

namespace App\Listeners\Invoice;

use Session;
use App\Quote;

use App\Events\Invoice\Created;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateQuote
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Created  $event
     * @return void
     */
    public function handle(Created $event)
    {
        Quote::updateQuoteInvoice(Session::getId(), $event->invoice);
    }
}
