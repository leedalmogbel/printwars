@extends('admin.partials.frame')

@section('content')
    <div class="card">
        <div class="card-header">Product Information</div>
        <div class="card-body">
            @include('globals.product.detail')
            <hr />
            <h2>Design Images</h2>
            <br />
            <table class="designs table table-striped table-bordered">
                <tr>
                    <th>Image</th>
                    <th><center>Side</center></th>
                    <th><center>Size</center></th>
                    <th><center>Dimension</center></th>
                    <th><center>Action</center></th>
                </tr>
                @foreach ($product->designImages('frontImg') as $design)
                    <tr>
                        <td><img src="{{$design['img']}}" /></td>
                        <td valign="middle">
                            <center>
                                <span class="text-primary">Front Side</span>
                            </center>
                        </td>
                        <td>
                            <center>
                                <span class="text-info">{{$design['size']}}</span>
                            </center>
                        </td>
                        <td>
                            <center>
                                <span class="text-success">{{ implode(' x ', $design['dimension']) }} pixels</span>
                            </center>
                        </td>
                        <td>
                            <center>
                                <a href="{{route('admin-design-download', "path={$design['img']}")}}">Download</a>
                            </center>
                        </td>
                    </tr>
                @endforeach
                @foreach ($product->designImages('backImg') as $design)
                    <tr>
                        <td><img src="{{$design['img']}}" /></td>
                        <td>
                            <center>
                                <span class="text-primary">Back Side</span>
                            </center>
                        </td>
                        <td>
                            <center>
                                <span class="text-primary">{{ $design['size'] }}</span>
                            </center>
                        </td>
                        <td>
                            <center>
                                <span class="text-success">{{ implode(' x ', $design['dimension']) }} pixels</span>
                            </center>
                        </td>
                        <td>
                            <center>
                                <a href="{{route('admin-design-download', 'path', 'asdf')}}">Download</a>
                            </center>
                        </td>
                    </tr>
                @endforeach
            </table>
            <br />
            <hr />
            <h2>Text Designs</h2>
            <br />
            <table class="table table-striped table-bordered txt-designs">
                <tr>
                    <th>Text</th>
                    <th><center>Side</center></th>
                    <th><center>Font</center></th>
                </tr>
                @foreach ($product->textDesigns('frontTxt') as $txt)
                    <tr>
                        <td>{{$txt->txt}}</td>
                        <td>
                            <center>
                                <span class="text-primary">Front Side</span>
                            </center>
                        </td>
                        <td>
                            <center>
                                <span class="text-warning">{{$txt->font}}</span>
                            </center>
                        </td>
                    </tr>
                @endforeach
                @foreach ($product->textDesigns('backTxt') as $txt)
                    <tr>
                        <td>{{$txt->txt}}</td>
                        <td>
                            <center>
                                <span class="text-primary">Back Side</span>
                            </center>
                        </td>
                        <td>
                            <center>
                                <span class="text-info">{{$txt->font}}</span>
                            </center>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection

@section('custom-css')
    <style>
     .product-img img {
         max-width: 100%;
     }
    </style>
@endsection

@section('custom-scripts')
    <script>
     window.page = 'product';
    </script>
@endsection
