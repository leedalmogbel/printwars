<?php

namespace App\Repos\User;

use App\User;
use App\Quote;

/**
 * User tier
 *
 */
class Tier
{
    private $user = null;
    private $tier = [
        [
            'name' => 'Tier 0',
            'discount' => 0,
            'count' => 0
        ], [
            'name' => 'Tier 1',
            'discount' => 60,
            'count' => 100,
        ], [
            'name' => 'Tier 2',
            'discount' => 80,
            'count' => 150
        ], [
            'name' => 'Tier 3',
            'discount' => 100,
            'count' => 200
        ], [
            'name' => 'Tier 4',
            'discount' => 120,
            'count' => '250'
        ]
    ];
    
    public function __construct(User $user) {
        if ($user->type != 'seller') {
            throw new \Exception('User is not a seller');
        }
        
        $this->user = $user;
    }

    /**
     * get seller tier
     *
     * @return array
     */
    public function getTier() {
        $orderCount = Quote::getDeliveredOrderBySellerThisMonth($this->user);
        $reverseTier = array_reverse($this->tier);

        foreach ($reverseTier as $tier) {
            if ($tier['count'] <= $orderCount) {
                return $tier;
            }
        }
    }

    /**
     * get tier discount
     *
     * @return numeric
     */
    public function getTierDiscount() {
        return $this->getTier()['discount'];
    }

    /**
     * get tier name
     *
     * @return string
     */
    public function getTIerName() {
        return $this->getTier()['name'];
    }
}