@extends('front.partials.frame')

@section('content')
    <br />
    <div class="order-list container">
        <h1>Orders</h1>
        <br />
        <table class="table table-striped table-bordered">
            <tr>
                <th><center>Order ID</center></th>
                <th><center>Total Amount</center></th>
                <th><center>Order Date</center></th>
            </tr>
            @foreach ($orders as $order)
                <tr>
                    <td>
                        <center><a href="{{route('checkout-order', $order->id)}}"># {{$order->id}}</a></center>
                    </td>
                    <td><center>PHP {{number_format($order->shipping_amount + $order->total_amount, 2)}}</center></td>
                    <td><center>{{date('M d, Y H:i', strtotime($order->created_at))}}</center></td>
                </tr>
            @endforeach
        </table>
        <br />
        {{$orders->links()}}
    </div>
@endsection
