<?php

namespace App\Providers;

use App\Events\Invoice\Created as InvoiceCreatedEvent;
use App\Events\Quote\Updating as QuoteUpdatingEvent;
use App\Events\Quote\Updated as QuoteUpdatedEvent;
use App\Events\User\Creating as UserCreatingEvent;
use App\Events\User\Created as UserCreatedEvent;

use App\Listeners\Invoice\SendConfirmation as SendInvoiceConfirmation;
use App\Listeners\Invoice\UpdateQuote as UpdateQuoteInvoice;
use App\Listeners\Quote\CheckQuoteStatus;
use App\Listeners\Quote\AddToSellerBalance;
use App\Listeners\User\CheckEmailDuplicate;
use App\Listeners\User\SendVerificationEmail;

use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        InvoiceCreatedEvent::class => [
            UpdateQuoteInvoice::class,
            SendInvoiceConfirmation::class,
        ],
        QuoteUpdatingEvent::class => [
            CheckQuoteStatus::class
        ],
        QuoteUpdatedEvent::class => [
            AddToSellerBalance::class
        ],
        UserCreatingEvent::class => [
            CheckEmailDuplicate::class
        ],
        UserCreatedEvent::class => [
            SendVerificationEmail::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
