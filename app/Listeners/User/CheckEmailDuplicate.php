<?php

namespace App\Listeners\User;

use App\User as UserModel;
use App\Events\User\Creating;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Validation\ValidationException;

class CheckEmailDuplicate
{
    const MSG = 'The email address `%s` is already taken.';
    
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Creating  $event
     * @return void
     */
    public function handle(Creating $event)
    {
        $exist = UserModel::where('email', $event->user->email)->first();
        
        if (empty($exist)) {
            return;
        }

        // TODO: should validate if email has been verified

        return $this->throwValidationException($event);
    }

    /**
     * throw validator message
     *
     * @param Creating $event
     * @return void
     */
    private function throwValidationException(Creating $event)
    {
        throw ValidationException::withMessages([
            'email' => sprintf(self::MSG, $event->user->email)
        ]);

    }
}
