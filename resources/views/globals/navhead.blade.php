<nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
    <a href="#" class="sidebar-toggler">
        <span class="navbar-toggler-icon"></span>
    </a>
    <a class="navbar-brand hide" href="{{ route('seller-dash') }}">GEAR <small class="brand-small">SHARK</small></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <div class="mr-auto"></div>
        <ul class="navbar-nav my-2 my-lg-0">
            <li class="nav-item active">
                <a class="nav-link" href="#"><span class="fa fa-home"></span></a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle"
                   href="#" id="navbarDropdown"
                   role="button" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false"><span class="fa fa-bell"></span></a>
                
                <div class="dropdown-menu dd-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#">Action</a>
                    <a class="dropdown-item" href="#">Another action</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Something else here</a>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#"><span class="fa fa-user"></span></a>
            </li>
        </ul>
    </div>
</nav>
