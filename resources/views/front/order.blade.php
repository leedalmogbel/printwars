@extends('front.partials.frame')

@section('content')
    <div class="invoice-order container">
        <br />
        <h1>Order NO: <span class="text-primary">#{{$invoice->id}}</span></h1>
        @if($invoice->daysPass() < 1 && $invoice->status != 0)
            <div class="float-right">
                <a href="#" data-id="{{$invoice->id}}" class="btn btn-danger cancel-order">&times; Cancel Order</a>
            </div>
        @endif
        <div class="text-secondary">Status:
            @if($invoice->status == 1)
                <span class="text-info">Pending</span>
            @elseif ($invoice->status == 2)
                <span class="text-warning">Processing</span>
            @elseif ($invoice->status == 3)
                <span class="text-primary">To be Shipped</span>
            @elseif ($invoice->status == 4)
                <span class="text-success">Delivered</span>
            @elseif ($invoice->status == 0)
                <span class="text-danger">Canceled</span>
            @else
                <span class="text-secondary">Unknown</span>
            @endif
        </div>
        <div><span class="text-secondary">Order Date:</span> {{date('M d, Y H:i', strtotime($invoice->created_at))}}</div>
        <hr />
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">Order Info</div>
                    <div class="card-body">
                        <div><strong>Name:</strong> {{$invoice->name}}</div>
                        <div><strong>Email Address:</strong> {{$invoice->email}}</div>
                        <div><strong>Phone:</strong> {{$invoice->phone}}</div>
                        <div><strong>Payment Method:</strong> {{$invoice->paymentMethodName()}}</div>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card">
                    <div class="card-header">Shipping Address</div>
                    <div class="card-body">
                        <div><strong>Street:</strong> {{$invoice->address('street')}}</div>
                        <div><strong>City:</strong> {{$invoice->address('city')}}</div>
                        <div><strong>Province:</strong> {{$invoice->address('province')}}</div>
                        <div><strong>Postal:</strong> {{$invoice->address('postal')}}</div>
                    </div>
                </div>
            </div>
        </div>
        <br />
        <div class="card order-packages">
            <div class="card-header">Packages</div>
            <div class="card-body">
                @foreach ($quotes as $item)
                    <div class="row">
                        <div class="col-md">
                            <img src="{{$item->product()->first()->image('front')}}" class="product-img" />
                        </div>
                        <div class="col-md">
                            <div>
                                <a href="{{$item->product()->first()->detailLink}}">
                                    <strong>{{$item->product()->first()->title}}</strong>
                                </a>
                            </div>
                            <div class="text-secondary">Sku:
                                <span class="text-danger">{{$item->product()->first()->sku}}</span>
                            </div>
                            <div class="text-secondary">Status:
                                <strong class="text-danger">
                                @if($item->status == '1')
                                    Pending
                                @else
                                    {{$item->resolveStatus()}}
                                @endif
                                </strong>
                            </div>
                            <div class="text-secondary">Size:
                                <span class="text-success">{{$item->actualSize()}}</span>
                            </div>
                            <div class="text-secondary">Qty:
                                <span class="text-success">x {{$item->qty}}</span>
                            </div>
                        </div>
                        <div class="col-md">
                            <div class="text-secondary">SRP:
                                <strong class="text-primary">PHP {{number_format($item->amount, 2)}}</strong>
                            </div>
                            <div class="text-secondary">Shipping Cost:
                                <strong class="text-primary">PHP {{number_format($item->shipping, 2)}}</strong>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <br />
                @endforeach
            </div>
        </div>
        <br />
        <div class="text-right">
            <div class="text-secondary">Shipping Total: <strong class="text-primary">{{number_format($invoice->shipping_amount, 2)}}</strong></div>
            <div class="text-secondary">Order Total: <strong class="text-primary">{{number_format($invoice->total_amount, 2)}}</strong></div>
            <div class="text-secondary">Grand Total: <strong class="text-primary">{{number_format($invoice->total_amount + $invoice->shipping_amount, 2)}}</strong></div>
        </div>
    </div>
@endsection
@section('custom-css')
    <link type="text/css" rel="stylesheet" href="/components/jquery-confirm/dist/jquery-confirm.min.css" />
@endsection
@section('custom-scripts')
    <script type="text/javascript" src="/components/jquery-confirm/dist/jquery-confirm.min.js"></script>
    <script>
     $('.cancel-order').click(function(e) {
         e.preventDefault();
         let val = $(this).data('id');
         $.confirm({
             'title' : 'Cancel Order',
             'type' : 'red',
             'content': 'Are you sure you want to cancel this order?',
             'buttons' : {
                 'Yes' : {
                     'btnClass' : 'btn btn-danger',
                     'action' : function () {
                         let url = '{{route('order-cancel', ':id')}}'.replace(':id', val);
                         window.location.href = url;
                     }
                 },
                 'Cancel' : function () {}
             }
         });
     });
    </script>
@endsection
