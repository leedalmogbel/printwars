<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('type')->default('buyer');
            $table->string('password');
            $table->string('photo');
            $table->timestamps();
            
            // index
            $table->index('name');
            $table->index('email');
            $table->index('email_verified_at');
            $table->index('type');
        });

        
        \DB::table('users')->insert(['name' => 'Admin Lee',
            'email' => 'admin@shirtshop.com',
            'email_verified_at' => date('Y-m-d H:i:s'),
            'type' => 'admin',
            'password' => md5('admin123!'),
            'photo' => '']
        );
        
        \DB::table('users')->insert(['name' => 'Seller 1',
            'email' => 'seller@shirtshop.com',
            'email_verified_at' => date('Y-m-d H:i:s'),
            'type' => 'seller',
            'password' => md5('seller123!'),
            'photo' => '']
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
