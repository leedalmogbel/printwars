/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});

const mobileWidth = 750;

window.isMobile = function () {
  if ($(window).width() <= mobileWidth) {
    return true;
  }

  return false;
};

window.hideSideBar = function () {
  $('.sidebar-content').css({width : 0});
  $('.main').css({marginLeft : 0});
  $('.main .navbar-brand').show();
};

window.showSideBar = function () {
  $('.sidebar-content').css({width : 250});
  if (!window.isMobile()) {
    $('.main').css({marginLeft : 250});
  }
  
  $('.main .navbar-brand').hide();
};

window.validateWidth = function () {
  if (window.isMobile()) {
    window.hideSideBar();
    return $('.close-sidebar').show();
  }

  window.showSideBar();
  return $('.close-sidebar').hide();
};

window.setPage = function () {
  if (typeof window.page == 'undefined' || !window.page) {
    return;
  }

  $('.sidebar-link li').removeClass('active');
  $(`.sidebar-link li[data-val="${window.page}"]`).addClass('active');
}

$('.close-sidebar').click(function (e) {
  e.preventDefault();
  window.hideSideBar();
});

$('.sidebar-toggler').click(function (e) {
  e.preventDefault();
  if($('.sidebar-content').width() > 0) {
    return window.hideSideBar();
  }

  return window.showSideBar();
});

window.validateWidth();
/*$(window).resize(function () {
  window.validateWidth();
});*/

$(document).ready(function () {
  window.setPage();
});
