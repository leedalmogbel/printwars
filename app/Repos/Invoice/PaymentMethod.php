<?php

namespace App\Repos\Invoice;

class PaymentMethod 
{
    private $methods = [
        'cod' => 'Cash on Delivery'
    ];

    /**
     * returns payment method name
     *
     * @param string $key
     * @return string
     */
    public function getName($key) {
        return $this->methods[$key];
    }

    /**
     * check if payment method exists
     *
     * @param string $key
     * @return bool
     */
    public function isValid($key) {
        return isset($this->methods[$key]);
    }

}