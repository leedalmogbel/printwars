<?php

namespace App\Http\Controllers\Front;

use Request;
use App\Http\Controllers\Controller;
use App\User as UserModel;
use Session;
use Redirect;

class User extends Controller
{
    const TYPE = 'buyer';
    
    //
    public function login () {
        return view('front.login');
    }
    
    public function signin () {
        $loginData = Request::validate(['email' => 'required|email',
            'password' => 'required'
        ]);

        try {
            // login
            $seller = UserModel::login($loginData);

            try {
                $u = $this->validateRedirectURL(Request::get('u'));
            } catch (\Exception $e) {
                $this->setToastrMsg($e->getMessage(), 'warning');

                $u = route('home');
            }
            
            return redirect($u);
        } catch (\Exception $e) {
            $this->setToastrMsg($e->getMessage(), 'error');
            
            return Redirect::route('front-login')->withInput(['email' => $loginData['email']])
                ->withErrors(['email' => $e->getMessage()]);
        }
    }

    public function signup() {
        return view('front.signup');
    }

    public function register() {
        $data = request()->validate([
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:6'
        ]);

        $data['type'] = self::TYPE;
        $data['password'] = UserModel::encryptPassword($data['password']);
        
        UserModel::create($data);

        $this->setToastrMsg('Registered successfully. Please login', 'success');
        return redirect()->route('front-login');
    }
}
