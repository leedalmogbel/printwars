<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Storage;

class Product extends Model
{
    const LOWEST_PRICE = 550;
    
    protected $fillable = [
        'title', 'description'
    ];

    protected static $status = ['approved' => 2,
        'rejected' => 0,
        'pending' => 1
    ];

    public function __get($var) {
        if ($var == 'detailLink') {
            return $this->detailLink();
        }

        if ($var == 'dashedTitle') {
            return $this->dashedTitle();
        }
        if ($var == 'htmlDescription') {
            return $this->htmlDescription();
        }

        return parent::__get($var);
    }

    public function user() {
        return $this->hasOne('App\User', 'id', 'user');
    }
    
    public function saveProduct($data, User $userObj) {
        // check title
        if (!trim($this->title)) {
            throw new \Exception('Title is required');
        }

        // check description
        if (!trim($this->description)) {
            throw new \Exception('Description is required');
        }

        // check front side
        if (!isset($data['frontSide'])) {
            throw new \Exception('Front side t-shirt design is not set');
        }

        if (!isset($data['backSide'])) {
            throw new \Exception('Back side t-shirt design is not set.');
        }

        if (!$userObj->id) {
            throw new \Exception('User is not set');
        }

        if (!$data['price'] || $data['price'] < self::LOWEST_PRICE) {
            throw new \Exception('Price is invalid');
        }

        try {
            $this->user = $userObj->id;
            $this->images = json_encode($this->saveImage($data));
            $this->designs = json_encode($this->saveDesigns($data));
            $txt = [];
            if (isset($data['frontTxt'])) {
                $txt['frontTxt'] = $data['frontTxt'];
            }

            if (isset($data['backTxt'])) {
                $txt['backTxt'] = $data['backTxt'];
            }

            $this->txt = json_encode($txt);
            $this->sku = $this->generateSKU();
            $this->active = 1;
            $this->status = 1;
            
            return $this->save();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function saveImage($imageData) {
        if (!isset($imageData['frontSide'])) {
            throw new \Exception('Front side t-shirt design is not set');
        }

        if (!isset($imageData['backSide'])) {
            throw new \Exception('Back side t-shirt design is not set');
        }
        
        $data = ['front' => 'tshirts/shirt-' . uniqid() . '.png',
            'back' => 'tshirts/shirt-' . uniqid() . '.png'];

        $frontImg = explode(',', $imageData['frontSide']);
        $backImg = explode(',', $imageData['backSide']);

        if (!isset($backImg[1]) || !isset($frontImg[1])) {
            throw new \Exception('Image data is invalid');
        }

        Storage::put("public/{$data['front']}", base64_decode($frontImg[1]));
        Storage::put("public/{$data['back']}", base64_decode($backImg[1]));

        return $data;
    }

    public function saveDesigns($designData) {
        $data = [];
        $keys = ['frontImg', 'backImg'];
        
        foreach ($keys as $key) {
            if (isset($designData[$key]) && is_array($designData[$key])) {
                foreach ($designData[$key] as $design) {
                    $name = 'designs/' . uniqid() . '.png';
                    $d = explode(',', $design);
                    if (!isset($d[1])) {
                        continue;
                    }

                    Storage::put("public/$name", base64_decode($d[1]));
                    $data[$key][] = $name;
                }
            }
        }

        return $data;
    }

    public static function getProductsByUser(User $user, $limit = 10) {
        if (!$user->id) {
            throw new \Exception('User is not set');
        }

        $query = self::where('user', $user->id)
            ->orderBy('updated_at', 'DESC');

        if (request()->has('q') && trim(request()->get('q')) != '') {
            $query->where('title', 'like', '%' . request()->get('q') . '%')
                  ->orWhere('sku', request()->get('q'));
        }
        
        return $query->paginate($limit);
    }

    public static function remove($id, User $user) {
        if (!$obj = self::isOwned($id, $user)) {
            throw new \Exception('Product doesn\'t exist');
        }

        $obj->active = 0;
        return $obj->save();
    }

    public function setStatus($status = 0) {
        if (empty($this)) {
            throw new \Exception('Product not found.');
        }

        $this->status = $status;
        return $this->save();
    }

    public static function activate($id, User $user) {
        if (!$obj = self::isOwned($id, $user)) {
            throw new \Exception('Product doesn\'t exist');
        }
        
        $obj->active = 1;
        return $obj->save();
    }

    public static function isOwned($id, User $user) {
        $obj = self::where('id', $id)
            ->where('user', $user->id)
            ->first();

        if (empty($obj)) {
            return false;
        }

        return $obj;
    }

    public function image ($index) {
        if (!$this->images) {
            return null;
        }

        try {
            $images = json_decode($this->images);
            return "/storage/{$images->$index}";
        } catch (\Exception $e) {
            return null;
        }
    }

    public static function getProductsByStatus($status, $user = null) {
        $products = self::orderBy('id', 'desc');

        if (isset(self::$status[$status])) {
            $products->where('status', self::$status[$status]);
        }

        if (request()->has('q') && trim(request()->get('q')) != '') {
            $products->where('title', 'like', '%' . request()->get('q') . '%')
                     ->orWhere('sku', request()->get('q'));
        }
        

        return $products->paginate(15);
    }

    public function seller () {
        return $this->hasOne('App\User', 'id', 'user');
    }

    public function designImages($index = 'frontImg') {
        if (empty($this)) {
            throw new \Exception('Product not found');
        }
        
        try {
            $designs = json_decode($this->designs)->$index;
        } catch (\Exception $e) {
            return [];
        }
        
        $d = [];
        foreach ($designs as $design) {
            list($w, $h) = getimagesize(public_path() . "/storage/$design");

            $size = filesize(public_path() . "/storage/$design");
            $units = array( 'B', 'KB', 'MB', 'GB');
            $power = $size > 0 ? floor(log($size, 1024)) : 0;
            $d[] = ['img' => "/storage/$design",
                'dimension' => [$w, $h],
                'size' =>  number_format($size / pow(1024, $power), 2, '.', ',') . ' ' . $units[$power]];
        }

        return $d;
    }

    public function generateSKU() {
        return 't-' . strtolower(uniqid());
    }

    public static function searchByKeyword ($keyword, $itemPerPage = 12) {
        if (!trim($keyword)) {
            throw new \Exception('Please provide a search word');
        }
        
        return self::selectRaw('*, match(title, description) against (? IN NATURAL LANGUAGE MODE) as score', [$keyword])
            ->whereRaw('match(title, description) against (? IN NATURAL LANGUAGE MODE) > 0', $keyword)
            ->where('status', 2)
            ->where('active', 1)
            ->orderBy('score', 'desc')
            ->paginate($itemPerPage);
    }

    public function textDesigns($index = 'frontTxt') {
        if (empty($this)) {
            throw new \Exception('Product not found');
        }

        try {
            return json_decode($this->txt)->$index;
        } catch (\Exception $e) {
            return [];
        }
    }

    /**
     * get best seller
     *
     * @return collection
     */
    public static function bestSeller() {
        return self::select('products.*')
            ->selectRaw('SUM(quotes.qty) as c')
            ->join('quotes', 'quotes.product', '=', 'products.id')
            ->where('quotes.invoice', '!=', '0')
            ->where('products.status', '=', '2')
            ->where('products.active', '=', '1')
            ->groupBy('products.id')
            ->orderBy('c', 'desc');
    }

    protected function dashedTitle() {
        if (!$this->title) {
            return null;
        }

        return preg_replace('/\s+/', '-', trim(strtolower($this->title)));
    }

    protected function detailLink() {
        if (!$this->id) {
            return null;
        }

        $title = $this->dashedTitle;
        
        return route('product-detail', [$this->id, $title]);
    }

    protected function htmlDescription() {
        if (!$this->description) {
            return null;
        }
        
        return str_replace("\n", "<br>", $this->description);
    }
}
