<?php

namespace App\Quote;

use Illuminate\Database\Eloquent\Model;
use App\Quote;

class Shipping extends Model
{
    const RATES = ['ncr' => 50,
        'luz' => 60,
        'vis' => 75,
        'min' => 75
    ];
    
    public static function calculate(Quote $quote, $region = 'ncr') {
        if (!isset(self::RATES[$region])) {
            throw new \Exception('Invalid region');
        }

        $qty = is_numeric($quote->qty) && $quote->qty ? $quote->qty : 1;
        return (self::RATES[$region] * $qty);
    }

    public static function updateRegion($session, $region = 'ncr') {
        $items = Quote::getCartItems($session);
        foreach ($items as $item) {
            $item->shipping = self::calculate($item, $region);
            $item->save();
        }

        return;
    }
}
