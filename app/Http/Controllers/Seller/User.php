<?php

namespace App\Http\Controllers\Seller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User as UserModel;
use Session;
use Redirect;

class User extends Controller
{
    const TYPE = 'seller';
    
    //
    public function login () {
        return view('seller.login', []);
    }

    public function signin (Request $request) {
        $loginData = $request->validate(['email' => 'required|email',
            'password' => 'required'
        ]);

        try {
            // login
            $seller = UserModel::login($loginData, self::TYPE);
            return Redirect::route('seller-dash');
        } catch (\Exception $e) {
            $this->setToastrMsg($e->getMessage(), 'error');
            return Redirect::route('seller-login')->withInput(['email' => $loginData['email']])
                ->withErrors(['email' => $e->getMessage()]);
        }
    }
}
