<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Session;
use App\Repos\User\Tier;

use App\Product;
use App\Events\User\Creating as UserCreatingEvent;
use App\Events\User\Created as UserCreatedEvent;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $dispatchesEvents = [
        'creating' => UserCreatingEvent::class,
        'created' => UserCreatedEvent::class,
    ];

    protected $guarded = [];

    /**
     * users wallet
     *
     * @return Model
     */
    public function wallet() {
        return $this->hasMany('App\Wallet', 'user', 'id');
    }

    /**
     * get user balance
     *
     * @return float
     */
    public function getBalance() {
        $latest = $this
            ->wallet()
            ->orderBy('id', 'desc')
            ->first();

        if (empty($latest)) {
            return 0;
        }

        return $latest->balance;
    }

    /**
     * login
     *
     * @param array data
     * @param string type
     * @return App\User
     */
    public static function login ($data, $type = null) {
        
        $user = User::where('email', $data['email'])
            ->where('password', self::encryptPassword($data['password']));
        
        if ($type) {
            $user->where('type', $type);
        }

        $user = $user->first();

        if (empty($user)) {
            throw new \Exception('Invalid username or password');
        }

        if ($type && $user->status != 2) {
            throw new \Exception('Your account is not yet active');
        }

        // set to session
        Session::put('me', $user);
        Session::save();

        return $user;
    }

    /**
     * get products by user id
     *
     * @param integer limit
     * @return App\Product
     */
    public function getProducts ($limit = 10) {
        if (!$this->id) {
            throw new \Exception('Unknown user');
        }

        return Product::getProductsByUser($this, $limit);
    }

    /**
     * user products
     *
     * @return App\Product
     */ 
    public function products() {
        return $this->hasMany('App\Product', 'user', 'id');
    }

    /**
     * get seller tier
     *
     * @return App\Repo\User\Tier
     */
    public function tier() {
        return new Tier($this);
    }

    /**
     * encrypt password
     *
     * @param string password
     * @return string
     */
    public static function encryptPassword($password) {
        return md5($password);
    }
}
