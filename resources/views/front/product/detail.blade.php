@extends('front.partials.frame')

@section('content')
    <br />
    <div class="container product-detail">
        <br />
        <div class="row">
            <div class="col-sm-7">
                <div class="product-detail-image-main">
                    <img src="{{$product->image('front')}}" />
                </div>
                <div class="product-detail-images">
                    <a href="#"><img src="{{$product->image('front')}}" /></a>
                    <a href="#"><img src="{{$product->image('back')}}" /></a>
                </div>
            </div>
            <div class="col-sm-5">
                <h1 class="product-detail-title">{{$product->title}}</h1>
                <div class="text-secondary">&mdash; sku <span class="text-danger">{{$product->sku}}</span></div>
                <div class="product-detail-seller text-secondary">
                    <span>&mdash; by <a href="{{route('seller-home', $product->seller()->first()->id)}}">{{$product->seller()->first()->name}}</a></span>
                </div>
                <br />
                @if ($product->status == 2 && $product->active == 1)
                <div class="product-detail-price">
                    <span class="text-success">PHP {{number_format($product->price, 2)}}</span>
                </div>
                @else
                <div class="product-detail-price">
                    <span class="text-danger">out of stock</span>
                </div>
                @endif
                <br />
                <form action="{{route('cart-add')}}" class="buy-box-wrapper">
                    <input type="hidden" name="id" value="{{$product->id}}" />
                    <div class="buy-box">
                        <div class="buy-box-detail">
                            <div><strong>More Detail:</strong></div>
                            <div><small>100% Cotton</small></div>
                            <br />
                            <div><strong>Sizes:</strong></div>
                            <br />
                            <table class="table">
                                <tr>
                                    <th>Size</th>
                                    <th>LENGTH</th>
                                    <th>SLEEVE</th>
                                    <th>WIDTH</th>
                                </tr>
                                <tr>
                                    <td>Extra Small</td>
                                    <td>26"</td>
                                    <td>7.5"</td>
                                    <td>16"</td>
                                </tr>
                                <tr>
                                    <td>Small</td>
                                    <td>28"</td>
                                    <td>8.13"</td>
                                    <td>18"</td>
                                </tr>
                                <tr>
                                    <td>Medium</td>
                                    <td>29"</td>
                                    <td>8.38"</td>
                                    <td>20"</td>
                                </tr>
                                <tr>
                                    <td>Large</td>
                                    <td>30"</td>
                                    <td>8.63"</td>
                                    <td>22"</td>
                                </tr>
                                <tr>
                                    <td>Extra Large</td>
                                    <td>31"</td>
                                    <td>8.88"</td>
                                    <td>24"</td>
                                </tr>
                            </table>
                        </div>
                        <hr />
                        <div class="row">
                            <div class="col-sm">
                                <p><strong>Select Size:</strong></p>
                                <div>
                                    <select class="form-control" name="size">
                                        <option value="xs">Extra Small</option>
                                        <option value="s">Small</option>
                                        <option value="m">Medium</option>
                                        <option value="l">Large</option>
                                        <option value="xl">Extra Large</option>
                                    </select>
                                </div>
                                <br />
                            </div>
                            <div class="col-sm">
                                <p><strong>Quantity:</strong></p>
                                <div>
                                    <input type="number"
                                           name="qty"
                                           class="form-control"
                                           value="1" />
                                </div>
                                <br />
                            </div>
                        </div>
                    </div>
                    @if ($product->status == 2 && $product->active == 1)
                    <button class="btn btn-primary add-to-cart-btn"><span class="fa fa-shopping-cart"></span> ADD TO CART</button>
                    @else
                    <button class="btn btn-danger add-to-cart-btn" disabled>OUT OF STOCK</button>
                    @endif
                </form>
                <br />
                <hr />
                <div>
                    <p><center><strong>Share this design</strong></center></p>
                    <div>
                        <center>
                            <a href="#" class="btn btn-outline-primary">
                                <span class="fab fa-facebook-f"></span> facebook
                            </a>
                            <a href="#" class="btn btn-outline-primary">
                                <span class="fab fa-twitter"></span> twitter
                            </a>
                        </center>
                    </div>
                </div>
            </div>
        </div>
        <br />
        <hr />
        <p>{!!$product->htmlDescription!!}</p>
    </div>
@endsection

@section('custom-scripts')
    <script type="text/javascript">
     $('.product-detail-images a').click(function(e) {
         e.preventDefault();
         let src = $(this).find('img').attr('src');
         $('.product-detail-image-main img').attr('src', src);
     });
    </script>
@endsection
