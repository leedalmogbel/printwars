<?php

namespace App\Listeners\Invoice;

use App\Quote;

use App\Events\Invoice\Created;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendConfirmation
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Created  $event
     * @return void²
     */
    public function handle(Created $event)
    {
        // TODO: send email to user
    }
}
