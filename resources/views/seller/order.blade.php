@extends('seller.partials.frame')

@section('content')
    @include('globals.orders', ['page' => 'seller'])
@endsection

@section('custom-css')
    <link type="text/css" rel="stylesheet" href="/components/jquery-confirm/dist/jquery-confirm.min.css" />
@endsection

@section('custom-scripts')
    <script type="text/javascript" src="/components/jquery-confirm/dist/jquery-confirm.min.js"></script>
    <script type="text/javascript">
     $('.cancel-order').click(function(e) {
         e.preventDefault();
         let id = $(this).data('id');
         $.confirm({
             title : 'Cancel Order',
             type : 'red',
             content : 'Are you sure you want to cancel this order?',
             buttons : {
                 Yes : {
                     btnClass : 'btn btn-danger',
                     action : function() {
                         window.location.href='{{route('seller-order-cancel', ':id')}}'.replace(':id', id);
                     }
                 },
                 Cancel : function() {}
             }
         });
     });

     @if ($status == 'payable')
     
     $('.check-select').change(function () {
         let payable = 0;
         $('.check-select').each(function() {
             if ($(this).is(':checked')) {
                 payable += $(this).data('payable');
             }
         });

         $('.bulk-payable a span').html(parseFloat(payable).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
     });

     $('.bulk-payable a').click(function(e) {
         e.preventDefault();
         id = [];
         $('.check-select').each(function() {
             if ($(this).is(':checked')) {
                 id.push('ids[]=' + $(this).val());
             }
         });
         
         let queryString = id.join('&');
         window.location.href = '{{route('seller-bulk-pay')}}?' + queryString;
     });
     
     @endif
    </script>
@endsection
