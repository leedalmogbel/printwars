<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Paypal;

class Payment extends Model
{
    const EXPRESS_CHECKOUT = 'express_checkout';
    const CURRENCY = 'PHP';

    /**
     * create payment
     *
     * @oaram numeric parent id
     * @param string type
     * @param App\User user
     * @return array
     */
    public static function createPayment($parent, $type = 'quotes', User $user) {
        $data = [];

        // check type
        switch ($type) {
            case 'quotes':
                // compose quote date
                $data = self::createQuotePayment($parent);
                break;
            case 'bulk':
                $data = self::createBulkPayment($parent, $user);
                break;
            default:
                throw new \Exception('Something went wrong');
                break;
        }

        // initialize providel
        $provider = \PayPal::setProvider(self::EXPRESS_CHECKOUT);
        // get provider response/
        $response = $provider->setCurrency(self::CURRENCY)->setExpressCheckout($data);

        // check if response is valid
        if (empty($response) || !isset($response['ACK'])) {
            throw new \Exception('Something went wrong. Please try again');
        }

        // check if response is success or fail
        if(strtolower(trim($response['ACK'])) != 'success') {
            throw new \Exception($response['L_LONGMESSAGE0']);
        }

        // init payment model
        $payment = new Payment;
        $payment->reference = $response['TOKEN'];
        $payment->status = 1;
        $payment->type = $type;
        $payment->user = $user->id;
        $payment->amount = $data['total'];
        $payment->description = 'Quote payment';
        $payment->meta = json_encode(['res' => $response]);
        $payment->save();
        
        return $response;
    }

    /**
     * cancel payment by token
     *
     * @param string token
     * @param string type
     * @param numeric parent
     * @return App\Payment
     */
    public static function cancelPaymentByToken($token, $type, $parent) {
        // init payment filter by token
        $payment = self::getPaymentByToken($token)
            ->where('type', $type)
            ->first();

        // check if payment exist
        if (empty($payment)) {
            throw new \Exception('Something went wrong');
        }

        // set status to cancel
        $payment->status = 0;
        $payment->save();
        return $payment;
    }

    /**
     * set payment success
     *
     * @param App\Quote quote
     * @param string token
     * @param string payerId
     * @return App\Payment
     */
    public static function paymentQuoteSuccess(Quote $quote, $token, $payerID) {
        // prepare quote data
        $data = self::createQuotePayment($quote);

        // prepare provider    
        $provider = \PayPal::setProvider(self::EXPRESS_CHECKOUT);

        // call provider
        $response = $provider->setCurrency(self::CURRENCY)
            ->doExpressCheckoutPayment($data, $token, $payerID);
        
        // check if response is valid
        if (empty($response) || !isset($response['ACK'])) {
            throw new \Exception('Something went wrong');
        }

        // check if response is successfull
        if(trim(strtolower($response['ACK'])) != 'success') {
            throw new \Exception($response['L_LONGMESSAGE0']);
        }

        // get patment by token
        $payment = self::getPaymentByToken($token)
            ->where('type', 'quotes')
            ->first();

        // check if payment exist
        if (empty($payment)) {
            throw new \Exception('Something went wrong');
        }

        // prepare meta
        $meta = json_decode($payment->meta, true);
        $meta['do'] = $response;
        $payment->meta = json_encode($meta);
        $payment->status = 2;
        
        $payment->save();

        // TODO: put this to event
        $quote->status = 2;
        $quote->payment = $payment->id;
        $quote->save();

        return $payment;
        
    }

    /**
     * get payment by token
     *
     * @param strng token
     * @return App\Payment
     */
    public static function getPaymentByToken($token) {
        return self::where('reference', $token);
    }

    /**
     * prepare quote data
     *
     * @param App\Quote quote
     * @return array
     */
    protected static function createQuotePayment(Quote $quote) {
        // check if quote is empty
        if (empty($quote)) {
            throw new \Exception('Something went wrong');
        }

        // check quote status
        if ($quote->status != 1) {
            throw new \Exception('You can no longer pay for this order');
        }

        // compose data
        $data = [];

        // order items
        $data['items'] = [
            ['name' => $quote->product()->first()->title,
                'price' => $quote->getPayable(),
                'desc'  => 'Payment to process shirt']
        ];

        // order info
        $data['invoice_id'] = uniqid();
        $data['invoice_description'] = "Order #{$quote->invoice} Invoice";
        $data['return_url'] = route('seller-payment-success', $quote->id);
        $data['cancel_url'] = route('seller-payment-cancel', $quote->id);
        $data['total'] = $quote->getPayable();

        return $data;
    }

    /**
     * create bulk payment
     *
     * @param array ids
     * @param App\User $user
     * @return array
     */
    public static function createBulkPayment($ids, User $user = null, $paymentID = null) {
        if (empty($ids)) {
            throw new \Exception('Invalid request');
        }

        if (!$user) {
            $user = \Session::get('me');
        }
        
        // compose data
        $data = $qs = [];
        $paymentID = !$paymentID ? self::getNextID() : $paymentID;
        $total = 0;
        $orderID = [];
        foreach ($ids as $id) {
            if ($id instanceof Quote) {
                $q = $id;
            } else {
                $q = Quote::find($id);
            }
            if (empty($q)) {
                throw new \Exception('Something went wrong');
            }

            if ($q->product()->first()->user != $user->id) {
                throw new \Exception('Invalid request');
            }
            if (Quote::statuses()->getValueByKey('payable') != $q->status) {
                throw new \Exception('Something went wrong');
            }
            
            $data['items'][] = [
                'name' => $q->product()->first()->title,
                'price' => $q->getPayable(),
                'desc' => 'Payment to process shirt'
            ];

            $qs[] = $q;
            $total += $q->getPayable();
            $orderID[] = $q->id;
        }

        foreach ($qs as $quote) {
            $quote->payment = $paymentID;
            $quote->save();
        }

        // order info
        $data['invoice_id'] = 'payment-sandbox-' . $paymentID;
        $data['invoice_description'] = "Order #{" . implode(',', $orderID) . "} Invoice";
        $data['return_url'] = route('seller-payment-bulk-success', $paymentID);
        $data['cancel_url'] = route('seller-payment-bulk-cancel', $paymentID);
        $data['total'] = $total;

        return $data;
    }

    /**
     * returns the next auto increment id value
     *
     * @return numeric
     */
    public static function getNextID() {
        $id = \DB::select("SHOW TABLE STATUS LIKE 'payments'");
        $id = $id[0]->Auto_increment;
        
        return $id; 
    }

    /**
     * payment success
     *
     * @param string PayerID
     */
    public function success($payerID) {
        if (empty($this)) {
            throw new \Exception('Something went wrong');
        }

        $ids = [];
        $quotes = Quote::where('payment', $this->id)
            ->get();

        // prepare quote data
        $data = self::createBulkPayment($quotes, null, $this->id);
        
        // prepare provider    
        $provider = \PayPal::setProvider(self::EXPRESS_CHECKOUT);

        // call provider
        $response = $provider->setCurrency(self::CURRENCY)
            ->doExpressCheckoutPayment($data, $this->reference, $payerID);

        // check if response is valid
        if (empty($response) || !isset($response['ACK'])) {
            throw new \Exception('Something went wrong');
        }

        // check if response is successfull
        if(trim(strtolower($response['ACK'])) != 'success') {
            throw new \Exception($response['L_LONGMESSAGE0']);
        }

        // prepare meta
        $meta = json_decode($this->meta, true);
        $meta['do'] = $response;
        $this->meta = json_encode($meta);
        $this->status = 2;
        
        $this->save();

        // TODO: put this to event
        foreach ($quotes as $quote) {
            $quote->status = 2;
            $quote->save();
        }

        return $this;
    }
}
