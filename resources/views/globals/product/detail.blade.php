<h1>{{$product->title}}</h1>
<div class="text-secondary">
    SKU: <span class="text-danger">{{$product->sku}}</span>
</div>
<div>PHP {{number_format($product->price, 2)}}</div>
<div>
    <span>status</span>:
                @if($product->active == 1)
                    <span class="text-success">Active</span>
                @else
                    <span class="text-danger">Inactive</span>
                @endif
                &mdash;
                @if($product->status == 1)
                    <span class="text-info">Pending</span>
                @elseif($product->status == 2)
                    <span class="text-success">Approved</span>
                @else
                    <span class="text-danger">Rejected</span>
                @endif
</div>
<hr />
<p>{!! str_replace("\n", "<br />", $product->description) !!}</p>
<br />
<div class="row product-img-admin">
    <div class="col-6">
        <img src="{{ $product->image('front') }}" />
    </div>
    <div class="col-6">
        <img src="{{ $product->image('back') }}" />
    </div>
</div>
