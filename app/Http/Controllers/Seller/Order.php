<?php

namespace App\Http\Controllers\Seller;

use Illuminate\Http\Request;
use Session;
use App\Http\Controllers\Controller;
use App\Quote;
use App\Payment;

class Order extends Controller
{
    //
    public function list($status) {
        try {
            $orders = Quote::getQuoteWithInvoiceBySeller(Session::get('me'), $status);
        } catch (\Exception $e) {
            $this->setToastrMsg($e->getMessage(), 'error');
            return redirect()->back();
        }

        return view('seller.order', [
            'orders' => $orders,
            'status' => $status,
        ]);
    }

    public function cancel(Quote $quote) {
        try {
            if (empty($quote)) {
                $this->setToastrMsg('Invalid cart item', 'error');
                return redirect()->back();
            }

            $quote->cancelItemBySeller(Session::get('me'));

            $this->setToastrMsg('Order canceled', 'success');
            return redirect()->back();

        } catch (\Exception $e) {
            $this->setToastrMsg($e->getMessage(), 'error');
            return redirect()->back();
        }
    }

    public function payNow(Quote $quote) {
        try {
            if (empty($quote)) {
                $this->setToastrMsg('Invalid cart item', 'error');
                return redirect()->back();
            }

            if ($quote->product()->first()->user != Session::get('me')->id) {
                $this->setToastrMsg('Something went wrong', 'error');
                return redirect()->back();   
            }

            $res = Payment::createPayment($quote, 'quotes', Session::get('me'));
            return redirect($res['paypal_link']);

        } catch (\Exception $e) {
            $this->setToastrMsg($e->getMessage(), 'error');
            return redirect()->back();
        }
    }

    public function bulkPayment() {
        try {
            if (!request()->has('ids') || !is_array(request()->get('ids'))) {
                throw new \Exception('Invalid request');
            }
            
            $res = Payment::createPayment(request()->get('ids'), 'bulk', Session::get('me'));
            return redirect($res['paypal_link']);
        } catch (\Exception $e) {
            $this->setToastrMsg($e->getMessage(), 'error');
            return redirect()->back();
        }
    }
}
