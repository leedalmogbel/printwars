<div class="col-sm-3">
    <div class="product-item">
        <div class="product-image">
            <a href="{{$product->detailLink}}"><img src="{{$product->image('front')}}"></a>
        </div>
        <div class="product-title">
            <a href="{{$product->detailLink}}">{{$product->title}}</a>
        </div>
        <div class="product-price">
            <small class="text-secondary">PHP {{number_format($product->price, 2)}}</small>
        </div>
    </div>
</div>
