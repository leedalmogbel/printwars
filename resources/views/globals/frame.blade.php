<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="{{$class}}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
        <!-- Fonts -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link href="/css/app.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="/components/fontawesome5-fullcss/css/all.css" />
        <link type="text/css" rel="stylesheet" href="/components/toastr/build/toastr.css" />
        <link type="text/css" rel="stylesheet" href="/components/pace-js/themes/red/pace-theme-flash.css" />
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <script type="text/javascript" src="/components/jquery/dist/jquery.min.js"></script>
        <script type="text/javascript" src="/components/toastr/build/toastr.min.js"></script>
        <script type="text/javascript" src="/components/pace-js/pace.min.js"></script>
        <script type="text/javascript" src="/components/html2canvas/dist/html2canvas.js"></script>
        <script type="text/javascript">
         toastr.options = {
             "positionClass": "toast-bottom-left",
         };
        </script>
        @yield('custom-css')
    </head>
    <body>
        <div class="sidebar-content">
            @include($class . '.partials.navside')
        </div>
        <div class="main">
            @include('globals.navhead')
            <div class="main-content">
            @yield('content')
            </div>
            <script type="text/javascript" src="/js/app.js"></script>
            @yield('custom-scripts')
            <script type="text/javascript">
             @if (Session::has('msg'))
             toastr['{{ Session::has('msg_type') ? Session::get('msg_type') : 'info' }}']('{{ Session::get('msg') }}');
             @endif
            </script>
        </div>
    </body>
</html>
