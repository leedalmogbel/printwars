<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Quote\Shipping;
use App\Quote;
use App\Events\Invoice\Created as InvoiceCreatedEvent;
use App\Repos\Invoice\PaymentMethod;

class Invoice extends Model
{
    
    const EMAIL_REGEX = '/^(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){255,})(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){65,}@)(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22))(?:\.(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-[a-z0-9]+)*\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-[a-z0-9]+)*)|(?:\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\]))$/iD';

    const MOBILE_REGEX = '/^(\+63|0)9\d\d\d\d\d\d\d\d\d$/';
    const PHONE_REGEX = '/^(\d{2,3})?\d\d\d\d\d\d\d$/';
    const PENDING_STATUS = 1;

    protected $dispatchesEvents = [
        'created' => InvoiceCreatedEvent::class
    ];

    /**
     * initializes App\Repos\Invoice\PaymentMethod
     *
     * @return App\Repos\Invoice\PaymentMethod
     */
    public static function paymentMethods() {
        return new PaymentMethod();
    }

    /**
     * get payment method name by key
     *
     * @return string
     */
    public function paymentMethodName() {
        if (!$this->method) {
            return;
        }

        return self::paymentMethods()->getName($this->method);
    }
    
    /**
     * get invoice list by user
     *
     * @param App\User $user
     * @return @collection
     */
    public static function getInvoicesByUser(User $user) {
        // validate user
        if (empty($user)) {
            throw new \Exception('Something went wrong');
        }

        // user needs to have id
        if (!$user->id) {
            throw new \Exception('Something went wrong');
        }

        // get invoices by user id
        return self::where('user', $user->id)
            ->orderBy('id', 'desc')
            ->paginate(10);
    }
    
    /**
     * create order by session id
     *
     * @param string session_id
     * @return $this
     */
    public function createOrder($sessionId) {
        // validate email
        if (!$this->isEmailValid()) {
            throw new \Exception('Email is invalid');
        }

        // check name
        if (!$this->checkName()) {
            throw new \Exception('Name is required');
        }

        // check phone number
        if (!$this->checkPhoneNumber()) {
            throw new \Exception('Phone number is invalid');
        }

        // check payment method
        if (!self::paymentMethods()->isValid($this->method)) {
            throw new \Exception('Invalid payment method');
        }

        $this->shipping = json_encode($this->prepareAddressObject());
        $this->reference = $this->composeUniqReference();
        $this->txnid = $this->composeUniqReference();
        $this->status = self::PENDING_STATUS;
        
        // get cart amount by session ud
        list($this->total_amount, $this->shipping_amount) = $this->getCartAmountInfo($sessionId);

        // Ssave invoice
        if(!$this->save()) {
            throw new \Exception('Something went wrong');
        }

        return $this;
    }

    /**
     * get address by index
     *
     * @param string $index
     * @return string
     */
    public function address($index = 'street') {
        // check if model is not empty
        if (empty($this)) {
            return '';
        }

        // decode json address
        $address = json_decode($this->shipping, true);
        // return address
        return $address[$index];
    }
    

    /**
     * day count order created
     *
     * @return numeric
     */
    public function daysPass() {
        $time = time() - strtotime($this->created_at);
        return $time / 86400;
    }

    /**
     * check if email is valid
     *
     * @return bool
     */
    protected function isEmailValid() {
        return $this->email && preg_match(self::EMAIL_REGEX, $this->email);
    }

    /*
     * trim name
     *
     * return string
     */
    protected function checkName() {
        return trim($this->name);
    }

    /**
     * check phone number format
     *
     * @return bool
     */
    protected function checkPhoneNumber() {
        return preg_match(self::PHONE_REGEX, $this->phone) || preg_match(self::MOBILE_REGEX, $this->phone);
    }

    /**
     * prepare address values
     *
     * @return array
     */
    protected function prepareAddressObject () {
        // validate address values
        if (!$this->street || !trim($this->street)) {
            throw new \Exception('Street address is invalid.');
        }

        if (!$this->city || !trim($this->city)) {
            throw new \Exception('City address is invalid');
        }

        if (!$this->province || !trim($this->province)) {
            throw new \Exception('Province address is invalid');
         }

        if (!$this->postal || !trim($this->postal)) {
            throw new \Exception('Postal address is invalid');
        }

        if (!$this->region || !trim($this->region) || !isset(Shipping::RATES[$this->region])) {
            throw new \Exception('Region address is invalid');
        }

        // push values to array
        $address = ['street' => $this->street,
            'city' => $this->city,
            'province' => $this->province,
            'postal' => $this->postal,
            'region' => $this->region
        ];

        // unset unused fields
        unset($this->street, $this->city, $this->province, $this->postal, $this->region);
        
        return $address;
    }

    /**
     * compose unique reference string
     *
     * @return string
     */
    protected function composeUniqReference() {
        return md5(uniqid());
    }

    /**
     * get cart amount
     *
     * @param string sessionId
     * @return array
     */
    protected function getCartAmountInfo($sessionId) {
        // get cart items by session id
        $cart = Quote::getCartItems($sessionId);

        return [$cart->totalAmount, $cart->shippingAmount];
    }

}
