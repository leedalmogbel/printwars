<div class="side">
    <a href="#" class="close-sidebar hide">&times;</a>
    <div class="side-logo">
        <a class="navbar-brand" href="{{ route('seller-dash') }}">GEAR <small class="brand-small">SHARK</small></a>
    </div>
    <ul class="sidebar-link">
        <li data-val="dashboard"><a href="{{ route('seller-dash') }}"><span class="fa fa-tachometer-alt"></span> Dashboard</a></li>
        <li data-val="product">
            <a href="{{ route('seller-product') }}"><span class="fas fa-tshirt"></span> Products</a>
            <ul class="sub-links">
                <li><a href="{{ route('seller-product-create') }}"><span class="fa fa-caret-right"></span> Upload a product</a></li>
                <li><a href="{{ route('seller-product-design') }}"><span class="fa fa-caret-right"></span> Design a Tshirt</a></li>
            </ul>
        </li>
        <li data-val="order"><a href="{{route('seller-order-list', 'all')}}"><span class="fas fa-tags"></span> Orders</a></li>
        <li data-val="setting"><a href="#"><span class="fas fa-cog"></span> Settings</a></li>
    </ul>
</div>
