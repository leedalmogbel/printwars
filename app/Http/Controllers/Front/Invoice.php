<?php

namespace App\Http\Controllers\Front;

use Request;
use App\Http\Controllers\Controller;
use Session;

use App\Quote;
use App\Quote\Shipping;
use App\Invoice as InvoiceModel;

class Invoice extends Controller
{
    //
    public function checkout() {
        if (!Session::has('me') || empty(Session::get('me'))) {
            $this->setToastrMsg('You need to login first before you can proceed to checkout page', 'error');
            return redirect()->route('front-login', ['u' => route('checkout')]);
        }

        $cart = Quote::getCartItems(Session::getId());
        if ($cart->isEmpty()) {
            $this->setToastrMsg('Nothing to checkout', 'error');
            return redirect()->route('cart');
        }

        $data = [];
        $data['cart'] = $cart;
        return view('front.checkout', $data);
    }

    public function updateRegion($region) {
        try {
            Shipping::updateRegion(Session::getId(), $region);
        } catch (\Exception $e) {
            $this->setToastrMsg($e->getMessage(), 'error');
            return redirect()->back();
        }
        
        return $this->checkout();
    }

    public function order() {
        $data = Request::validate(['email' => 'required|email',
            'contact' => 'required',
            'name' => 'required',
            'street' => 'required',
            'city' => 'required',
            'province' => 'required',
            'postal' => 'required|numeric',
            'region' => 'required',
            'payment' => 'required'
        ]);

        try {
            $invoice = new InvoiceModel();
            $invoice->email = $data['email'];
            $invoice->phone = $data['contact'];
            $invoice->name = $data['name'];
            $invoice->street = $data['street'];
            $invoice->city = $data['city'];
            $invoice->province = $data['province'];
            $invoice->postal = $data['postal'];
            $invoice->region = $data['region'];
            $invoice->method = $data['payment'];
            $invoice->user = Session::get('me')->id;

            $invoice = $invoice->createOrder(Session::getId());
            
            $this->setToastrMsg('Order successfully created', 'success');
            
            return redirect()->route('checkout-order', $invoice->id);
        } catch (\Exception $e) {
            $this->setToastrMsg($e->getMessage(), 'error');
            return redirect()->back()->withInput($data);
        }
    }

    public function invoiceOrder(InvoiceModel $invoice) {
        if (empty($invoice)) {
            abort(404);
        }

        if (!Session::has('me')) {
            abort(404);
        }
        
        if ($invoice->user != Session::get('me')->id) {
            abort(404);
        }

        $quotes = Quote::getQuoteByInvoice($invoice);
        return view('front.order', ['invoice' => $invoice,
            'quotes' => $quotes]);
    }

    public function orders() {
        if (!Session::has('me')) {
            abort(404);
        }

        try {
            $orders = InvoiceModel::getInvoicesByUser(Session::get('me'));
        } catch(\Exception $e) {
            $this->setToastrMsg($e->getMessage(), 'error');
            return redirect()->back();
        }
        
        return view('front.orders', ['orders' => $orders]);
    }

    public function cancel(InvoiceModel $invoice) {
        if (empty($invoice)) {
            $this->setToastrMsg('Invalid Order', 'error');
            return redirect()->back();
        }

        if (!Session::has('me')) {
            $this->setToastrMsg('Invalid Order', 'error');
            return redirect()->back();
        }

        if (Session::get('me')->id != $invoice->user) {
            $this->setToastrMsg('Invalid Order', 'error');
            return redirect()->back();
        }

        if ($invoice->daysPass() > 1) {
            $this->setToastrMsg('Cannot cancel this order anymore', 'error');
            return redirect()->back();
        }

        try {
            $invoice->status = 0;
            $invoice->save();
            
        } catch (\Exception $e) {
            $this->setToastrMsg($e->getMessage(), 'error');
            return redirect()->back();
        }

        $this->setToastrMsg('Order updated successfully', 'success');
        return redirect()->back();
    }
}
