@extends('seller.partials.frame')

@section('content')
    <div class="card">
        <div class="card-header">Product Information</div>
        <div class="card-body">
            @include('globals.product.detail')
        </div>
    </div>
@endsection

@section('custom-css')
    <style>
     .product-img img {
         max-width: 100%;
     }
    </style>
@endsection

@section('custom-scripts')
    <script>
     window.page = 'product';
    </script>
@endsection
