<?php

namespace App\Events\User;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use App\User as UserModel;

class Creating
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    //
    public $user;
    
    /**
     * Create a new event instannce.
     *
     * @return void
     */
    public function __construct(UserModel $user)
    {
        $this->user = $user;
    }
}
