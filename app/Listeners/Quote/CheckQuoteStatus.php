<?php

namespace App\Listeners\Quote;

use App\Events\Quote\Updating as QuoteUpdatingEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Quote;

class CheckQuoteStatus
{

    const ERROR_MSG = 'Cannot move from %s to %s';
    
    private $rules = [
        '1' => ['0', '2'],
        '2' => ['3'],
        '3' => ['4'],
        '4' => ['5']
    ];
    
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  QuoteUpdating  $event
     * @return void
     */
    public function handle(QuoteUpdatingEvent $event)
    {
        $currentStatus = $event->quote->getOriginal('status');
        $newStatus = $event->quote->status;

        // check if transaction is updating the status
        if ($currentStatus == $newStatus) {
            return;
        }
        
        if (!$this->checkRules($currentStatus, $newStatus)) {
            throw new \Exception(sprintf(self::ERROR_MSG, $currentStatus, $newStatus));
        }
    }

    /**
     * check if arguments follows rules
     *
     * @param numeric current status
     * @param numeric new status
     * @return bool
     */
    private function checkRules($current, $new) {
        return isset($this->rules[$current])
            && in_array($new, $this->rules[$current]);
    }
}
