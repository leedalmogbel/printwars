<?php

namespace App\Http\Controllers\Seller;

use Request;
use Session;
use App\Http\Controllers\Controller;
use App\Product as ProductModel;
use App\User;

class Product extends Controller
{
    public function index () {
        return view('seller.product', ['products' => Session::get('me')->getProducts()]);
    }

    public function detail($id) {
        if(!$product = ProductModel::isOwned($id, Session::get('me'))) {
            $this->setToastrMsg('Product doesn\'t exist', 'error');
            return redirect()->back();
        }
        
        return view('seller.product.detail', ['product' => $product]);
    }
    
    public function design () {
        return view('seller.product.design');
    }

    public function create () {
        return view('seller.product.create');
    }

    public function remove($id) {
        try {
            ProductModel::remove($id, Session::get('me'));
            $this->setToastrMsg('Product deactivated', 'success');
        } catch (\Exception $e) {
            $this->setToastrMsg($e->getMessage(), 'error');
        }
        
        return redirect()->back();
    }
    
    public function activate($id) {
        try {
            ProductModel::activate($id, Session::get('me'));
            $this->setToastrMsg('Product activated', 'success');
        } catch (\Exception $e) {
            $this->setToastrMsg($e->getMessage(), 'error');
        }
        
        return redirect()->back();
    }

    public function store () {
        $data = Request::validate(['frontSide' => 'required',
            'backSide' => 'required',
            'title' => 'required',
            'price' => 'required',
            'description' => 'required']);

        try {
            $product = new ProductModel();
            $product->title = $data['title'];
            $product->description = $data['description'];
            $product->price = $data['price'];

            if (Request::has('frontImg')) {
                $data['frontImg'] = Request::get('frontImg');
            }

            if (Request::has('backImg')) {
                $data['backImg'] = Request::get('backImg');
            }

            if (Request::has('frontTxt')) {
                $data['frontTxt'] = Request::get('frontTxt');
            }

            if (Request::has('backTxt')) {
                $data['backTxt'] = Request::get('backTxt');
            }
            
            $product->saveProduct($data, Session::get('me'));
            $this->setToastrMsg('Product saved successfully', 'success');
            
        } catch (\Exception $e) {
            $this->setToastrMsg($e->getMessage(), 'error');
        }

        return redirect()->back();
    }
}
