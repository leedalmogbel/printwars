<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class CheckLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $type)
    {
        if (!Session::exists('me') || empty(Session::get('me'))) {
            Session::flash('msg', 'Need to login first');
            Session::flash('msg_type', 'error');
            return redirect("/$type/login");
        }

        if ($type != Session::get('me')->type) {
            Session::flash('msg', 'Invalid permission');
            Session::flash('msg_type', 'error');
            return redirect("/$type/login");
        }
        
        return $next($request);
    }
}
