<div class="side">
    <a href="#" class="close-sidebar hide">&times;</a>
    <div class="side-logo">
        <a class="navbar-brand" href="{{ route('admin-dash') }}">GEAR <small class="brand-small">SHARK</small></a>
    </div>
    <ul class="sidebar-link">
        <li data-val="dashboard"><a href="{{ route('admin-dash') }}"><span class="fa fa-tachometer-alt"></span> Dashboard</a></li>
        <li data-val="product">
            <a href="{{ route('admin-product', 'all') }}"><span class="fas fa-tshirt"></span> Designs</a>
        </li>
        <li data-val="seller"><a href="#"><span class="fas fa-users"></span> Sellers</a></li>
        <li data-val="order"><a href="{{route('admin-order-list', 'all')}}"><span class="fas fa-tags"></span> Orders</a></li>
        <li data-val="setting"><a href="#"><span class="fas fa-cog"></span> Settings</a></li>
    </ul>
</div>
