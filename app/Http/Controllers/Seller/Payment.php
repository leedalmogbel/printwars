<?php

namespace App\Http\Controllers\Seller;

use Request;
use App\Http\Controllers\Controller;
use App\Quote;
use Session;
use App\Payment as PaymentModel;

class Payment extends Controller
{
    public function cancelQuote(Quote $quote) {
        if (empty($quote)) {
            $this->setToastrMsg('Something went wrong', 'error');
            return redirect()->route('seller-order-list', 'all');
        }

        if ($quote->product()->first()->user != Session::get('me')->id) {
            $this->setToastrMsg('Something went wrong', 'error');
            return redirect()->route('seller-order-list', 'all');
        }

        if (!Request::has('token')) {
            $this->setToastrMsg('Invalid request', 'error');
            return redirect()->route('seller-order-list', 'all');
        }

        try {
            PaymentModel::cancelPaymentByToken(Request::get('token'), 'quotes', $quote->id);

            $this->setToastrMsg('Payment has been cancelled', 'warning');
            return redirect()->route('seller-order-list', 'payable');
        } catch (\Exception $e) {
            $this->setToastrMsg($e->getMessage(), 'error');
            return redirect()->route('seller-order-list', 'payable');
        }
    }

    public function successQuote(Quote $quote) {
        if (empty($quote)) {
            $this->setToastrMsg('Something went wrong', 'error');
            return redirect()->route('seller-order-list', 'payable');
        }

        if ($quote->product()->first()->user != Session::get('me')->id) {
            $this->setToastrMsg('Something went wrong', 'error');
            return redirect()->route('seller-order-list', 'payable');
        }

        if (!Request::has('token') || !Request::has('PayerID')) {
            $this->setToastrMsg('Invalid request', 'error');
            return redirect()->route('seller-order-list', 'payable');
        }

        try {
            $payment = PaymentModel::paymentQuoteSuccess($quote, Request::get('token'), Request::get('PayerID'));

            $this->setToastrMsg('Payment successfull', 'success');
            return redirect()->route('seller-order-list', 'payable');
        } catch (\Exception $e) {
            $this->setToastrMsg($e->getMessage(), 'error');
            return redirect()->route('seller-order-list', 'payable');
        }
    }

    public function cancel(PaymentModel $payment) {
        try {
            if (empty($payment)) {
                throw new \Exception('Something went wrong');
            }

            if ($payment->user != Session::get('me')->id) {
                throw new \Exception('Something went wrong');
            }

            if ($payment->status == 2) {
                throw new \Exception('This transaction cannot be cancelled anymore');
            }

            $payment->status = 0;
            $payment->save();

            $this->setToastrMsg('Payment cancelled', 'warning');
            return redirect()->route('seller-order-list', 'payable');
        } catch (\Exception $e) {
            $this->setToastrMsg($e->getMessage(), 'error');
            return redirect()->route('seller-order-list', 'payable');
        }
    }

    public function success(PaymentModel $payment) {
        try {
            if (empty ($payment)) {
                throw new \Exception('Something went wrong');
            }

            if ($payment->user != Session::get('me')->id) {
                throw new \Exception('Something went wrong');
            }

            $payment->success(Request::get('PayerID'));
            
            $this->setToastrMsg('Payment successfull', 'success');
            return redirect()->route('seller-order-list', 'payable');
            
        } catch (\Exception $e) {
            $this->setToastrMsg($e->getMessage(), 'error');
            return redirect()->route('seller-order-list', 'payable');
        }
        
    }
}
